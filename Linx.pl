#!/usr/local/bin/perl

# DEPEDENCIES:
#=========================
# databases/p5-DBD-Pg
# devel/p5-Config-General
# mail/p5-Email-Sender
# misc/p5-Locale-Codes
# security/p5-Digest-MD5
# www/p5-CGI-Session
# www/p5-CGI.pm
# www/p5-URI-Encode

package Linx;


use strict;
use warnings;
use locale;
#use utf8;

use Encode qw();
use CGI qw(-autoload);
use DBI qw();
use Digest::MD5 qw();
use CGI::Session qw(-ip-match);
use Email::Sender::Simple qw(sendmail);
use Email::Simple qw();
use File::Temp qw();
use POSIX qw();
use Data::Dumper qw();
use Time::HiRes qw();
use Locale::Country qw();
use Config::General qw(ParseConfig);
use URI::Encode qw(uri_decode);

my $exec_start_time = Time::HiRes::time();


$Data::Dumper::Terse = 1;
$Data::Dumper::Indent = 0;


my %linx = (
	 version			=> '0.8.4'
	,author				=> 'Aldis Berjoza'
	,copyright			=> 'Copyright &copy; 2011, Aldis Berjoza'
	,name				=> 'Linx'
);


my $cgi = CGI->new;

my %default_settings = (
	 linx_db_conn				=> 'dbi:Pg:dbname=links;host=127.0.0.1;port=5432'
	,linx_db_user				=> 'links'
	,linx_db_password			=> ''
	,allow_register_new_users	=> 1
	,min_password_len			=> 8
	,min_age_required			=> 14
	,max_age_assumed			=> 110

	,links_per_page				=> 15
	,max_links_per_page			=> 100
	,min_links_per_page			=> 10

	,request_timeout			=> 48	# hours
	,email_from					=> 'no-reply@bsdroot.lv'
	,base_dir					=> '' # must not end with slash
	,host						=> $cgi->url(-base => 1)
	,spamtrap_email				=> 'a@bsdroot.lv'
	,session_db					=> 'tmp/session.db'
	,tmp_dir					=> 'tmp'
	,keywords					=> 'linx, links'
	,title						=> 'Linx'

	,max_links_for_tag			=> 20
	,min_links_for_tag			=> 1
	,min_pub_links_for_tag		=> 10
	,links_for_tag				=> 1		# minumum links for tag to be shown in tagmap

	,theme						=> 'dark-blue'
	,fallback_theme				=> 'dark-blue'
	,devel						=> 1
	,show_url					=> 1
	,order_tags					=> 0
	,tagmap_on_top				=> 0
);


my @dec2short_table = (
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
	'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
	'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
	'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z'
);
my %short2dec_table = (
	0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, a=>10, b=>11,
	c=>12, d=>13, e=>14, f=>15, g=>16, h=>17, i=>18, j=>19, k=>20, l=>21,
	m=>22, n=>23, o=>24, p=>25, q=>26, r=>27, s=>28, t=>29, u=>30, v=>31,
	w=>32, x=>33, y=>34, z=>35, A=>36, B=>37, C=>38, D=>39, E=>40, F=>41,
	G=>42, H=>43, I=>44, J=>45, K=>46, L=>47, M=>48, N=>49, O=>50, P=>51,
	Q=>52, R=>53, S=>54, T=>55, U=>56, V=>57, W=>58, X=>59, Y=>60, Z=>61,
);
my $short_base_n = 62;

my %settings = ParseConfig("linx.conf");

while (my ($key, $value) = each(%default_settings)) {
	$settings{$key} = $value unless(exists($settings{$key}));
}

my %user_settings = (
	 theme 				=> $settings{theme}
	,min_links_for_tag	=> $settings{min_links_for_tag}
	,links_per_page		=> $settings{links_per_page}
	,show_url			=> $settings{show_url}
	,order_tags			=> $settings{order_tags}
	,tagmap_on_top		=> $settings{tagmap_on_top}
);

my %field_limits = (
	 username_max_len			=> 32
	,password_max_len			=> 22
	,password_null_hash			=> '1B2M2Y8AsgTpgAmY7PhCfg'
	,email_max_len				=> 256
	,name_max_len				=> 64
	,surname_max_len			=> 64
	,city_max_len				=> 64
	,country_max_len			=> 3
	,request_max_len			=> 32
	,confirm_key_max_len		=> 32
	,request_data_max_len		=> 256
	,tag_max_len				=> 64
	,url_max_len				=> 4096
	,url_title_max_len			=> 512
	,url_description_max_len	=> 16384
	,alias_max_len				=> 32
);



&print_postgresql_queries() if (defined($ARGV[0]) && $ARGV[0] eq '--generate-sql');

my ($session_dir, $session_file) = $settings{session_db} =~ m#^(.*/)??(.+)$#;
my $session = new CGI::Session('driver:db_file', $cgi, {Directory => $session_dir, FileName => $session_file}, {name => 'SID'});
my $user_id = $session->param('user_id') || 0;
my $remote_addr = $ENV{REMOTE_ADDR} || '0.0.0.0';

my $page = Encode::decode_utf8(URI::Encode::uri_decode($cgi->url(-path_info => 1, -absolute => 1),1)) || '/';
$page =~ s/^$settings{base_dir}//;


my $db = DBI->connect($settings{linx_db_conn}, $settings{linx_db_user}, $settings{linx_db_password}, {AutoCommit => 1});
my $pub = $cgi->url_param('pub') || 0;

if ($user_id) {
	$session->expire('+3d');
	my ($stm, $res);

	my $settings = eval $session->param('settings');
	if (defined $settings && $settings) {
		while (my ($key, $value) = each(%{$settings})) {
			$user_settings{$key} = $value;
		}
	}

	$res = $db->do('UPDATE users SET last_visit = now(), last_ip = ? WHERE id = ? AND password = ? AND active = true', undef, $remote_addr, $user_id, $session->param('password'));
	if ($res == 0E0) {
		$session->delete();
		$session->flush();
		$user_id = 0;
	}
} else {
	$pub = 1;
}

unless ($user_id) {
	$session->expire('+15m');
	&action_register()		if ($page eq '/Register' && $settings{allow_register_new_users});
	&action_confirm()		if ($page eq '/Confirm');
	&action_chpass()		if ($page eq '/ChPass');
	&action_redir()			if ($page =~ m#^/R[a-zA-Z0-9]+$#);
	&action_links()			if ($page eq '/');
	&action_search()		if ($page eq '/Search');
	&print_search_plugin()	if ($page eq '/SearchPlugin');
	&action_tag()			if ($page =~ lc($page));

	&action_login();
	&Exit();
}


&action_links()			if ($page eq '/');
&action_add()			if ($page eq '/Add');
&action_chemail()		if ($page eq '/ChEmail');
&action_chpass()		if ($page eq '/ChPass');
&action_confirm()		if ($page eq '/Confirm');
&action_deleteaccount()	if ($page eq '/DeleteAccount');
&action_edit()			if ($page eq '/Edit');
&action_import()		if ($page eq '/Import');
&acton_links()			if ($page eq '/Links');
&action_logout()		if ($page eq '/Logout');
&action_preferences()	if ($page eq '/Preferences');
&action_redir()			if ($page =~ m#^/R[a-zA-Z0-9]+$#);
&action_redir()			if ($page =~ m#^/A/(\w+)$#);
&action_search()		if ($page eq '/Search');
&action_tagless()		if ($page eq '/Tagless');
&action_trash()			if ($page eq '/Trash');
&action_userdata()		if ($page eq '/UserData');
&print_search_plugin()	if ($page eq '/SearchPlugin');
&action_tag()			if ($page =~ lc($page));
&action_not_faund();

# Action {{{1

sub action_deleteaccount() { # {{{2
	my @errors;
	my ($stm, $res, $row);
	my ($new_email, $new_email2, $password, $password2, $email, $name, $surname);

	if ($cgi->param('submit')) {
		$password = Digest::MD5::md5_base64($cgi->param('password'));
		$cgi->param(-name => 'password', -value => '');

		if ($#errors eq -1) {
			$stm = $db->prepare('SELECT password, name, surname, email FROM users WHERE id = ?');
			$res = $stm->execute($user_id);
			if ($res != 0E0) {
				$row = $stm->fetch();
				$password2	= $row->[0];
				$name		= $row->[1];
				$surname	= $row->[2];
				$email		= $row->[3];

				push(@errors, 'Invalid password') if ($password ne $password2);
			} else {
				push(@errors, 'Invalid password');
			}
			$password = $password2 = '';
		}
	}

	if ($#errors ne -1 || !$cgi->param('submit')) {
		&print_head;
		&print_errors(@errors);

		print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/DeleteAccount", -enctype => 'multipart/form-data', -id => 'chemail', -class => 'fancy');
		print $cgi->start_fieldset();
		print $cgi->label('Password'), '<br/>';
		print $cgi->password_field(-class => 'w', -name => 'password');
		print $cgi->end_fieldset();
		print $cgi->submit(-name => 'submit', -value => 'Ok');
		print $cgi->end_form();
		&print_tail();
		&Exit();
	}


	my $confirm_key = Digest::MD5::md5_hex(rand() . time(). rand());
	$res = $db->do("INSERT INTO user_requests (user_id, request, confirm_key) VALUES (?, 'deleteaccount', ?)", undef, $user_id, $confirm_key);
	if ($res != 0E0) {
		$stm = $db->prepare("SELECT id FROM user_requests WHERE user_id = ? AND request = 'deleteaccount' AND confirm_key = ? LIMIT 1");
		$res = $stm->execute($user_id, $confirm_key);
		if ($res != 0E0) {
			$row = $stm->fetch();
			my $request_id = $row->[0];

			my $email_msg = Email::Simple->create(
				header => [
					To      => "\"$name $surname\" <$email>",
					From    => "<$settings{email_from}>",
					Subject => 'Account deletion request',
				]
				,body => <<EOF
Hello, $name $surname

You have requested to delete your account
In order to change delete your account visit link below within 48 hours since registering.
$settings{host}$settings{basedir}/Confirm?action=deleteaccount&id=$request_id&key=$confirm_key

If you didn't request to delete your account and/or received this e-mail by accident,
please fallow this link
$settings{host}$settings{basedir}/Confirm?action=deleteaccount&id=$request_id&key=$confirm_key&cancel=1
EOF
			);
			sendmail($email_msg);

			&print_message_and_redir(title => 'Account deletion requested', message => 'Confirmation email was sent to you. In order to delete your account you need to visit link in email withing 48 hours from now.', timeout => 10, target => '/');
		} else {
			push(@errors, 'Request failed 1'); # FIXME
		}
	} else {
		push(@errors, 'Request failed 2'); # FIXME
	}


	# TODO

	&print_head;
	&print_errors(@errors);
	&print_tail();
	&Exit();

} # 2}}}

sub action_chemail() { # {{{2
	my @errors;
	my ($stm, $res, $row);
	my ($new_email, $new_email2, $password, $password2, $email, $name, $surname);

	if ($cgi->param('submit')) {
		$new_email = lc(Encode::decode_utf8(URI::Encode::uri_decode($cgi->param('new_email'), 1)));
		$new_email2 = lc(Encode::decode_utf8(URI::Encode::uri_decode($cgi->param('new_email2'), 1)));
		$password = Digest::MD5::md5_base64($cgi->param('password'));
		$cgi->param(-name => 'password', -value => '');

		unless ($new_email =~ m/^[\w\.\+\-=]+@[\w\.\-]+\.[\w\-]+$/) {
			push(@errors, 'Invalid e-mail');
			$new_email = $new_email2 = $password;
			$cgi->param(-name => 'new_email', -value => '');
			$cgi->param(-name => 'new_email2', -value => '');
		}
		if ($new_email ne $new_email2) {
			push(@errors, "E-mails doesn't match");
			$new_email = $new_email2 = $password = '';
			$cgi->param(-name => 'new_email', -value => '');
			$cgi->param(-name => 'new_email2', -value => '');
		}

		if ($#errors eq -1) {
			$stm = $db->prepare('SELECT count(email) FROM users WHERE email = ?');
			$res = $stm->execute($new_email);
			if ($res != 0E0) {
				$row = $stm->fetch();
				if ($row->[0]) {
					push(@errors, "Email address '$new_email' is already used.");
					$cgi->param(-name => 'email', -value => '');
					$cgi->param(-name => 'email2', -value => '');
				}
			} else {
				push(@errors, 'Query failed');
			}

			$stm = $db->prepare('SELECT password, name, surname, email FROM users WHERE id = ?');
			$res = $stm->execute($user_id);
			if ($res != 0E0) {
				$row = $stm->fetch();
				$password2	= $row->[0];
				$name		= $row->[1];
				$surname	= $row->[2];
				$email		= $row->[3];

				push(@errors, 'Invalid password') if ($password ne $password2);
			} else {
				push(@errors, 'Invalid password');
			}
			$password = $password2 = '';
		}
	}

	if ($#errors ne -1 || !$cgi->param('submit')) {
		&print_head;
		&print_errors(@errors);

		print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/ChEmail", -enctype => 'multipart/form-data', -id => 'chemail', -class => 'fancy');
		print $cgi->start_fieldset();
		print $cgi->label('New e-mail address'), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'new_email', -maxlength => $field_limits{email_max_len}), '<br/>';
		print $cgi->label('New e-mail address again'), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'new_email2', -maxlength => $field_limits{email_max_len}), '<br/>';
		print $cgi->end_fieldset();
		print $cgi->start_fieldset();
		print $cgi->label('Password'), '<br/>';
		print $cgi->password_field(-class => 'w', -name => 'password');
		print $cgi->end_fieldset();
		print $cgi->submit(-name => 'submit', -value => 'Ok');
		print $cgi->end_form();
		&print_tail();
		&Exit();
	}


	my $confirm_key = Digest::MD5::md5_hex(rand() . time(). rand());
	$res = $db->do("INSERT INTO user_requests (user_id, request, confirm_key, data) VALUES (?, 'chemail', ?, ?)", undef, $user_id, $confirm_key, $new_email);
	if ($res != 0E0) {
		$stm = $db->prepare("SELECT id FROM user_requests WHERE user_id = ? AND request = 'chemail' AND confirm_key = ? LIMIT 1");
		$res = $stm->execute($user_id, $confirm_key);
		if ($res != 0E0) {
			$row = $stm->fetch();
			my $request_id = $row->[0];

			my $email_msg = Email::Simple->create(
				header => [
					To      => "\"$name $surname\" <$email>",
					From    => "<$settings{email_from}>",
					Subject => 'e-mail change requested',
				]
				,body => <<EOF
Hello, $name $surname

You have requested to change your email
In order to change your password visit link below within 48 hours since registering.
$settings{host}$settings{basedir}/Confirm?action=chemail&id=$request_id&key=$confirm_key

If you didn't request to change email and/or received this e-mail by accident,
please fallow this link
$settings{host}$settings{basedir}/Confirm?action=chemail&id=$request_id&key=$confirm_key&cancel=1
EOF
			);
			sendmail($email_msg);

			&print_message_and_redir(title => 'e-mail change requested', message => 'Confirmation email was sent to you. In order to change your password you need to visit link in email withing 48 hours from now.', timeout => 10, target => '/');
		} else {
			push(@errors, 'Request failed 1'); # FIXME
		}
	} else {
		push(@errors, 'Request failed 2'); # FIXME
	}


	# TODO

	&print_head;
	&print_errors(@errors);
	&print_tail();
	&Exit();

} # 2}}}

sub action_userdata() { # {{{2
	my @errors;
	my @cctime = localtime;
	my $this_year = $cctime[5] + 1900;
	my ($stm, $res, $row);
	my ($name, $surname, $day, $month, $year, $sex, $country, $city, $bdate);

	unless ($cgi->param('submit')) {
		$stm = $db->prepare('SELECT name, surname, sex, bdate, country, city FROM users WHERE id = ?');
		$stm->execute($user_id);
		$row = $stm->fetch();

		$name		= $row->[0];
		$surname	= $row->[1];
		$sex		= $row->[2];
		$bdate		= $row->[3];
		$country	= Locale::Country::code2country($row->[4]);
		$city		= $row->[5];

		($year, $month, $day) = $bdate =~ m/(\d+)-(\d+)-(\d+)/;
		$month	=~ s/^0//;
		$day	=~ s/^0//;
	}

	if ($cgi->param('submit')) {
		# checks
		$name			= $cgi->param('name') 			|| '';
		$surname		= $cgi->param('surname') 		|| '';
		$day			= $cgi->param('day') 			|| '';
		$month			= $cgi->param('month') 			|| '';
		$year			= $cgi->param('year') 			|| '';
		$sex			= $cgi->param('sex') 			|| '';
		$country		= $cgi->param('country') 		|| '';
		$city			= $cgi->param('city') 			|| '';

		if (length($name) < 2) {
			push(@errors, 'Invalid name');
			$name = '';
			$cgi->param(-name => 'name', -value => '');
		}
		if (length($surname) < 2) {
			push(@errors, 'Invalid surname');
			$surname = '';
			$cgi->param(-name => 'surname', -value => '');
		}
		unless ($sex =~ m/[MF]/) {
			push(@errors, 'No sex');
			$sex = '?';
			$cgi->param(-name => 'sex', -value => '');
		}
		if ($day < 1 || $day > 31) {
			push(@errors, 'Invalid day of birth date');
			$day = 0;
			$cgi->param(-name => 'day', -value => '');
		}
		if ($month < 1 || $month > 12) {
			push(@errors, 'Invalid month of birth date');
			$month = 0;
			$cgi->param(-name => 'month', -value => '');
		}
		if ($year > $this_year || $year < $this_year - $settings{max_age_assumed}) {
			push(@errors, 'Invalid year of birth date');
			$year = 0;
			$cgi->param(-name => 'year', -value => '');
		}
		if ($year < $this_year - $settings{max_age_assumed}) {
			push(@errors, 'You are too young for this. Go out and play with your Friends');
			$year = 0;
			$cgi->param(-name => 'year', -value => '');
		}
		unless (Locale::Country::country2code($country)) {
			push(@errors, 'Invalid country');
			$country = '';
			$cgi->param(-name => 'country', -value => '');
		}
		if (length($city) < 3) {
			push(@errors, 'Invalid city');
			$city = '';
			$cgi->param(-name => 'city', -value => '');
		}
	}


	if ($#errors ne -1 || !$cgi->param('submit')) {
		&print_head;
		&print_errors(@errors);

		print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/UserData", -enctype => 'multipart/form-data', -id => 'userdata', -class => 'fancy');

		print $cgi->start_fieldset(), $cgi->legend('Personal info');

		print $cgi->label('Name', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'name', -maxlength => $field_limits{name_max_len}, -default => $name), '<br/>';
		print $cgi->label('Surname', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'surname', -maxlength => $field_limits{surname_max_len}, -default => $surname), '<br/>';

		print $cgi->label('Date of birth', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->label('Day: ');
		print $cgi->popup_menu(
			 -name		=> 'day'
			,-values	=> [0 .. 31]
			,-labels	=> {0 => ''}
			,-default	=> $day
		);
		print $cgi->label(' Month: ');
		print $cgi->popup_menu(
			 -name		=> 'month'
			,-values	=> [0 .. 12]
			,-labels	=> {
				0 => '', 1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
				7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'
			}
			,-default	=> $month
		);
		print $cgi->label(' Year: ');
		print $cgi->popup_menu(
			 -name		=> 'year'
			,-values	=> ['', sort { $b <=> $a } $this_year - $settings{max_age_assumed} .. $this_year]
			,-default	=> $year
		), '<br/>';

		print $cgi->label('Sex', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->popup_menu(
			 -name		=> 'sex'
			,-values	=> ['?', 'F', 'M']
			,-labels	=> {'?' => '', F => 'Female', M => 'Male'}
			,-default	=> $sex
		), '<br/>';

		print $cgi->label('Country', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';

		my @countries = Locale::Country::all_country_names();
		print $cgi->popup_menu(
			 -name		=> 'country'
			,-values	=> \@countries
			,-default	=> $country
		), '<br/>';

		print $cgi->label('City', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'city', -maxlength => $field_limits{city_max_len}, -default => $city), '<br/>';
		print $cgi->end_fieldset();

		print $cgi->submit(-name => 'submit', -value => 'Update');

		print $cgi->end_form();
		&print_tail();
		&Exit();
	}


	$bdate = "$year-$month-$day";
	$res = $db->do('UPDATE users SET name = ?, surname = ?, bdate = ?, sex = ?, city = ?, country = ? WHERE id = ?', undef, $name, $surname, $bdate, $sex, $city, Locale::Country::country2code($country), $user_id);
	if ($res == 0E0) {
		&print_message_and_redir(title => 'Update failed', message => 'Failed to update profile', timeout => 3, target => '/UserData');
	}
	&redir('/');
	&Exit();
} # 2}}}

sub action_search() { # {{{2 FIXME needs lost of improvement. For some reason SELECT INTO doesn't seam to work correctly
	my ($stm, $res, $row);
	my @errors;

	my $query = $cgi->param('search');
	my $search_terms = $query;

	URI::Encode::uri_decode($query, 1);
	$query =~ s/ /|/g;
#	$stm = $db->prepare("SELECT * INTO TEMP TABLE search_$user_id FROM links WHERE user_id = ? AND deleted = false AND to_tsvector(title || ' ' || coalesce(description) || ' ' || url) @@ to_tsquery(?)");
#	$res = $stm->execute($user_id, $query);
#	if ($res != 0E0) {

#		$stm = $db->prepare("SELECT count(id) FROM search_$user_id");
#		$res = $stm->execute();
		unless ($pub) {
			$stm = $db->prepare("SELECT count(id) FROM links WHERE user_id = ? AND deleted = false AND to_tsvector(title || ' ' || coalesce(description) || ' ' || url) @@ to_tsquery(?)");
			$res = $stm->execute($user_id, $query);
		} else {
			$stm = $db->prepare("SELECT count(id) FROM links WHERE public = true AND deleted = false AND to_tsvector(title || ' ' || coalesce(description) || ' ' || url) @@ to_tsquery(?)");
			$res = $stm->execute($query);

		}
		if ($res != 0E0) {
			$row = $stm->fetch();
			my $link_count = $row->[0];

			my $page = &real_page();

#			$stm = $db->prepare("SELECT id, title, description, url FROM search_$user_id ORDER BY hits DESC, url ASC LIMIT ? OFFSET ?");
#			$res = $stm->execute($user_settings{links_per_page}, $page * $user_settings{links_per_page});
			unless ($pub) {
				$stm = $db->prepare("SELECT id, title, description, url FROM links WHERE user_id = ? AND deleted = false AND to_tsvector(title || ' ' || coalesce(description) || ' ' || url) @@ to_tsquery(?) ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?");
				$res = $stm->execute($user_id, $query, $user_settings{links_per_page}, $page * $user_settings{links_per_page});
			} else {
				$stm = $db->prepare("SELECT id, title, description, url FROM links WHERE public = true AND deleted = false AND to_tsvector(title || ' ' || coalesce(description) || ' ' || url) @@ to_tsquery(?) ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?");
				$res = $stm->execute($query, $user_settings{links_per_page}, $page * $user_settings{links_per_page});
			}

			if ($res != 0E0) {
				&print_head;

				&print_tagmap() if $user_settings{tagmap_on_top};

				print $cgi->start_div({-class => 'container'});
				print $cgi->h1("Search result for '$search_terms'");
				unless ($pub) {
					&print_links(links => \$stm, link_count => $link_count, show_tags => 1, form_buttons => $cgi->submit(-name => 'delete', -value => 'Delete'));
				} else {
					&print_links(links => \$stm, link_count => $link_count, show_tags => 1);
				}
				print $cgi->end_div(); # div.container
				&print_tagmap() unless $user_settings{tagmap_on_top};

				&print_tail();
				&Exit();
			} else {
				push(@errors, 'Failed to retrieve links.'); # TODO improve message
			}
		} else {
			push(@errors, 'Failed to retrieve link count.'); # TODO improve message
		}
#	} else {
#		push(@errors, 'Search failed');
#	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

sub action_chpass() { # {{{2
	my @errors;
	my ($stm, $res, $row);

	if ($cgi->param('submit')) {
		my $email = lc(Encode::decode_utf8($cgi->param('email')));
		$cgi->param('email', '');

		if ($user_id) {
			$stm = $db->prepare('SELECT id, name, surname FROM users WHERE email = ? AND active = true AND id = ?');
			$res = $stm->execute($email, $user_id);
		} else {
			$stm = $db->prepare('SELECT id, name, surname FROM users WHERE email = ? AND active = true');
			$res = $stm->execute($email);
		}
		if ($res != 0E0) {
			$row = $stm->fetch();
			my $user_id	= $row->[0];
			my $name	= $row->[1];
			my $surname	= $row->[2];
			if ($user_id > 0) {

				my $confirm_key = Digest::MD5::md5_hex(rand() . time(). rand());
				$res = $db->do("INSERT INTO user_requests (user_id, request, confirm_key) VALUES (?, 'chpass', ?)", undef, $user_id, $confirm_key);
				if ($res != 0E0) {
					$stm = $db->prepare("SELECT id FROM user_requests WHERE user_id = ? AND request = 'chpass' AND confirm_key = ? LIMIT 1");
					$res = $stm->execute($user_id, $confirm_key);
					if ($res != 0E0) {
						$row = $stm->fetch();
						my $request_id = $row->[0];

						my $email_msg = Email::Simple->create(
							header => [
								To      => "\"$name $surname\" <$email>",
								From    => "<$settings{email_from}>",
								Subject => 'Password change requested',
							]
							,body => <<EOF
Hello, $name $surname

You have requested new password
In order to change your password visit link below within 48 hours since registering.
$settings{host}$settings{basedir}/Confirm?action=chpass&id=$request_id&key=$confirm_key

If you didn't request to change password and/or received this e-mail by
accident, please fallow link below
$settings{host}$settings{basedir}/Confirm?action=chpass&id=$request_id&key=$confirm_key&cancel=1
EOF
						);
						sendmail($email_msg);

						&print_message_and_redir(title => 'New password requested', message => 'Confirmation email was sent to you. In order to change your password you need to visit link in email withing 48 hours from now.', timeout => 10, target => '/');
					} else {
						push(@errors, 'Request failed 1'); # FIXME
					}
				} else {
					push(@errors, 'Request failed 2'); # FIXME
				}

			} else {
				push(@errors, 'Invalid e-mail address');
			}
		} else {
			push(@errors, 'Invalid e-mail address or deactivated account');
		}
	}

	&print_head;
	&print_errors(@errors);

	print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/ChPass", -enctype => 'multipart/form-data', -id => 'chpass', -class => 'fancy');
	print $cgi->start_fieldset();
	print $cgi->label('Your e-mail'), '<br/>';
	print $cgi->textfield(-class => 'w', -name => 'email', -maxlength => $field_limits{email_max_len});
	print $cgi->end_fieldset();
	print $cgi->submit(-name => 'submit', -value => 'Ok');
	print $cgi->end_form();
	&print_tail();
	&Exit();
} # 2}}}

sub action_trash() { # {{{2
	my @errors;
	my ($stm, $res, $row);

	if ($cgi->param('restore') || $cgi->param('delete')) {
		if ($cgi->param('restore')) {
			$stm = $db->prepare('UPDATE links SET deleted = false WHERE user_id = ? AND id = ?');
		}
		elsif ($cgi->param('delete')) {
			$stm = $db->prepare('DELETE FROM links WHERE user_id = ? AND deleted = true AND id = ?');
		}

		my @links = grep(m/^l[A-Za-z0-9]+$/, $cgi->param());
		foreach my $link_id (@links) {
			$link_id =~ s/^l//;
			$stm->execute($user_id, &short2dec($link_id));
		}
	}

	$stm = $db->prepare('SELECT count(id) FROM links WHERE user_id = ? AND deleted = true');
	$res = $stm->execute($user_id);
	unless ($res == 0E0) {
		$row = $stm->fetch();
		my $link_count = $row->[0];

		my $page = &real_page();

		$stm = $db->prepare('SELECT id, title, description, url FROM links WHERE user_id = ? AND deleted = true ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?');
		$res = $stm->execute($user_id, $user_settings{links_per_page}, $page * $user_settings{links_per_page});
		unless ($res == 0E0) {
			&print_head;
			&print_errors(@errors);

			&print_tagmap() if $user_settings{tagmap_on_top};

			print $cgi->start_div({-class => 'container'});
			print $cgi->h1('Trash');

			my $form_buttons = $cgi->submit(-name => 'delete', value => 'Delete');
			$form_buttons .=  $cgi->submit(-name => 'restore', value => 'Restore');

			&print_links(links => \$stm, link_count => $link_count, show_tags => 1, form_buttons => $form_buttons);

			print $cgi->end_div(); # div.container

			&print_tagmap() unless $user_settings{tagmap_on_top};

			&print_tail();
			&Exit();
		} else {
			push(@errors, 'Trash is empty');
		}
	} else {
		push(@errors, 'Failed to retrieve link count.');
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

sub action_tagless() { # {{{2
	my @errors;
	my ($stm, $res, $row);

	if ($cgi->param('delete')) {
		my @lp = $cgi->param();
		@lp = grep(m/^l[A-Za-z0-9]+$/, @lp);

		$stm = $db->prepare('UPDATE links SET deleted = true WHERE user_id = ? AND id = ?');

		foreach my $link_id (@lp) {
			$link_id =~ s/^l//;
			$stm->execute($user_id, &short2dec($link_id));
		}
	}

	# temp table seems slower

	$stm = $db->prepare('SELECT COUNT(id) FROM links WHERE user_id = ? AND deleted = false AND id <> ALL (SELECT link_id FROM tagmap WHERE user_id = ?)');
	$res = $stm->execute($user_id, $user_id);
	if ($res != 0E0 && $res) {
		$row = $stm->fetch();
		my $link_count = $row->[0];

		my $page = &real_page();

		$stm = $db->prepare('SELECT id, title, description, url FROM links WHERE user_id = ? AND deleted = false AND id <> ALL (SELECT link_id FROM tagmap WHERE user_id = ?) ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?');
		$res = $stm->execute($user_id, $user_id, $user_settings{links_per_page}, $page * $user_settings{links_per_page});
		if ($res != 0E0) {
			&print_head;
			print $cgi->start_div({-class => 'container'});
			print $cgi->h1('Links without tag');

			&print_links(links => \$stm, link_count => $link_count, show_tags => 0, form_buttons => $cgi->submit(-name => 'delete', -value => 'Delete'));
			print $cgi->end_div(); # div.container

			&print_tagmap();

			&print_tail();
			&Exit();

		} else {
			push(@errors, 'No tagles links'); # TODO improve message
		}
	} else {
		push(@errors, 'No tagles links'); # TODO improve message
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

sub action_tag() { # {{{2
	my @errors;
	my ($stm, $res, $row);

	if ($cgi->param('delete') && !$pub) {
		my @lp = $cgi->param();
		@lp = grep(m/^l[A-Za-z0-9]+$/, @lp);

		$stm = $db->prepare('UPDATE links SET deleted = true WHERE user_id = ? AND id = ?');

		foreach my $link_id (@lp) {
			$link_id =~ s/^l//;
			$stm->execute($user_id, &short2dec($link_id));
		}
	}

	my $tag = Encode::decode_utf8(URI::Encode::uri_decode($cgi->url(-path_info=>1, -absolute =>1), 1));
	$tag =~ s#^$settings{base_dir}/##;
	$tag =~ s#//#/#g;
	$tag =~ s#/$##;

	my @tags = split(m#/#, $tag);
	my ($query, $exclude_query);
	unless($pub) {
		$query = 'SELECT link_id FROM tagmap WHERE user_id = ? AND tag = ?';
		$exclude_query = 'SELECT tag FROM tagmap WHERE user_id = ? AND tag = ?';
	} else {
		$query = 'SELECT link_id FROM tagmap WHERE tag = ?';
		$exclude_query = 'SELECT tag FROM tagmap WHERE tag = ?';
	}

	for (my $i = 0; $i < $#tags; $i++) {
		$query = 'SELECT link_id FROM tagmap WHERE link_id = ANY (' . $query . ') AND tag = ?';
		$exclude_query .= ' OR tag = ?';
	}

	$stm = $db->prepare("SELECT COUNT(link_id) FROM ($query) AS x");
	unless ($pub) {
		$res = $stm->execute($user_id, @tags);
	} else {
		$res = $stm->execute(@tags);
	}
	if ($res != 0E0) {
		$row = $stm->fetch();
		my $link_count = $row->[0];

		my $page = &real_page();

		unless ($pub) {
			$stm = $db->prepare("SELECT id, title, description, url FROM links WHERE id = ANY ($query) AND user_id = ? AND deleted = false ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?");
			$res = $stm->execute($user_id, @tags, $user_id, $user_settings{links_per_page}, $page * $user_settings{links_per_page});
		} else {
			$stm = $db->prepare("SELECT id, title, description, url FROM links WHERE id = ANY ($query) AND public = true AND deleted = false ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?");
			$res = $stm->execute(@tags, $settings{links_per_page}, $page * $settings{links_per_page});
		}
		if ($res != 0E0) {
			&print_head;
			print $cgi->start_div({-class => 'container'});
			print $cgi->start_div({-id => 'tagdir'});
			print 'TAG: ', $cgi->a({-href => "$settings{base_dir}/"}, '/');
			my $tagd = '';
			foreach my $mtag (@tags) {
				$tagd .= "/$mtag";
				print $cgi->a({-href => "$settings{base_dir}$tagd"}, "$mtag/");
			}
			print $cgi->end_div(); # div#tagdir



			# subtags
			my $stm2;
			unless ($pub) {
				$stm2 = $db->prepare("SELECT tag, COUNT(link_id) AS links FROM tagmap WHERE tag <> all ($exclude_query) AND user_id = ? AND link_id = any ($query) GROUP BY tag ORDER BY tag");
				$res = $stm2->execute($user_id, @tags, $user_id, $user_id, @tags);
			} else {
				$stm2 = $db->prepare("SELECT tag, COUNT(link_id) AS links FROM tagmap WHERE tag <> all ($exclude_query) AND link_id = any ($query) GROUP BY tag ORDER BY tag");
				$res = $stm2->execute(@tags, @tags);
			}
			if ($res != 0E0) {
				my (@subtags, %subtag_links);

					my $max = 0;
					while ($row = $stm2->fetch()) {
						push(@subtags, $row->[0]);
						$subtag_links{$row->[0]} = $row->[1];
						$max = $row->[1] if $max < $row->[1];
					}

					print $cgi->start_div({-id => 'subtagmap'});
					print 'Subtags: ';
					foreach my $subtag (@subtags) {
						my $ratio = $subtag_links{$subtag} / $max;

						if		($ratio > 0.7)	{ print $cgi->a({-class => 't0', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						elsif	($ratio > 0.3)	{ print $cgi->a({-class => 't1', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						elsif	($ratio > 0.2)	{ print $cgi->a({-class => 't2', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						elsif	($ratio > 0.1)	{ print $cgi->a({-class => 't3', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						elsif	($ratio > 0.05)	{ print $cgi->a({-class => 't4', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						elsif	($ratio > 0.03)	{ print $cgi->a({-class => 't5', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						elsif	($ratio > 0.02)	{ print $cgi->a({-class => 't6', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }
						else					{ print $cgi->a({-class => 't7', -href => "$settings{base_dir}/$tag/$subtag"}, $subtag); }

						print ' '; # important!!!
					}
					print $cgi->end_div(); # div#subtagmap
			}
			#end of subtagmap

			unless ($pub) {
				&print_links(links => \$stm, link_count => $link_count, show_tags => 1, form_buttons => $cgi->submit(-name => 'delete', -value => 'Delete'));
			} else {
				&print_links(links => \$stm, link_count => $link_count, show_tags => 1);
			}

			print $cgi->end_div(); # div.container

			&print_tail();
			&Exit();

		} else {
			push(@errors, "No such tag: $tag");
		}
	} else {
		push(@errors, 'Query failed. This is probably some kind of bug :(');
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

sub action_preferences() { # {{{2
	my @errors;
	my ($links_per_page, $theme, $min_links_for_tag, $show_url, $order_tags, $tagmap_on_top);
	my ($stm, $res, $row);

	opendir(THEME_DIR, 'themes');
	my @themes = readdir(THEME_DIR);
	closedir(THEME_DIR);
	chomp(@themes);
	@themes = sort(grep(!m/\..*/, @themes));

	if ($cgi->param('submit')) {
		$links_per_page		= $cgi->param('links_per_page');
		$min_links_for_tag	= $cgi->param('min_links_for_tag');
		$theme				= $cgi->param('theme');
		$show_url			= $cgi->param('show_url');
		$order_tags			= $cgi->param('order_tags');
		$tagmap_on_top		= $cgi->param('tagmap_on_top');

		if ($links_per_page > $settings{max_links_per_page} || $links_per_page < $settings{Min_links_per_page}) {
			push(@errors, 'Links per page out of range');
		} else {
			$user_settings{theme} = $theme if ($cgi->param('submit') eq 'Preview');
		}
		push(@errors, 'Invalid theme') unless (grep(/$theme/, @themes));
		push(@errors, 'Min links for tag to be displayed in tagmap out of range')
			if ($min_links_for_tag < $settings{min_links_for_tag} || $min_links_for_tag > $settings{max_links_for_tag});
	}

	if ($cgi->param('submit') eq 'OK' && $#errors == -1) {
		$user_settings{theme}				= $theme;
		$user_settings{min_links_for_tag}	= $min_links_for_tag;
		$user_settings{links_per_page}		= $links_per_page;
		$user_settings{show_url}			= $show_url || 0;
		$user_settings{order_tags}			= $order_tags || 0;
		$user_settings{tagmap_on_top}		= $tagmap_on_top || 0;

		my $settings = Data::Dumper::Dumper(\%user_settings);
		$session->param('settings', $settings);

		$res = $db->do('UPDATE users SET settings = ? WHERE id = ?', undef, $settings, $user_id);
		if ($res != 0E0) {
			&redir('/');
			&Exit();
		} else {
			push(@errors, 'Failed to update settings');
		}
	}

	&print_head();
	&print_errors(@errors);
	print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/Preferences", -enctype => 'multipart/form-data', -id => 'settings', -class => 'fancy');
	print $cgi->start_fieldset();
	print $cgi->hidden(-name => 'referer', -value => $ENV{'HTTP_REFERER'}) if defined $ENV{'HTTP_REFERER'};
	print $cgi->label('Theme: ');

	print $cgi->popup_menu(
		-name => 'theme'
		,-values => \@themes
		,-default => $user_settings{theme}
	);
	print ' ';
	print $cgi->submit(-name => 'submit', -value => 'Preview');

	print '<br/>';
	print $cgi->label('Links per page: ');

	my @links_per_page_array;
	for (my $i = $settings{min_links_per_page}; $i <= $settings{max_links_per_page}; $i += 5) {
		push(@links_per_page_array, $i);
	}

	print $cgi->popup_menu(
		-name => 'links_per_page'
		,-values => \@links_per_page_array
		,-default => $user_settings{links_per_page}
	);

	print '<br/>';
	print $cgi->label('Min links for tag to be displayed in tagmap: ');
	print $cgi->popup_menu(
		-name => 'min_links_for_tag'
		,-values => [$settings{min_links_for_tag} .. $settings{max_links_for_tag}]
		,-default => $user_settings{min_links_for_tag}
	);

	print '<br/>';
	print $cgi->checkbox(-name => 'show_url', -value => 1, -checked => $user_settings{show_url}, -label => 'Show url below title');
	print '<br/>';
	print $cgi->checkbox(-name => 'order_tags', -value => 1, -checked => $user_settings{order_tags}, -label => 'Order tags below link (Disable to improve performace)');
	print '<br/>';
	print $cgi->checkbox(-name => 'tagmap_on_top', -value => 1, -checked => $user_settings{tagmap_on_top}, -label => 'Display tagmap on top of links');

	print $cgi->end_fieldset();
	print $cgi->submit(-name => 'submit', -value => 'OK');
	print $cgi->end_form();

	&print_tail();
	&Exit;
} # 2}}}

sub action_links() { # {{{2
	my @errors;
	my ($stm, $res, $row);

	if ($cgi->param('delete') && !$pub) {
		my @lp = $cgi->param();
		@lp = grep(m/^l[A-Za-z0-9]+$/, @lp);

		$stm = $db->prepare('UPDATE links SET deleted = true WHERE user_id = ? AND id = ?');

		foreach my $link_id (@lp) {
			$link_id =~ s/^l//;
			$stm->execute($user_id, &short2dec($link_id));
		}
	}


	unless($pub) {
		$stm = $db->prepare('SELECT count(id) FROM links WHERE user_id = ? AND deleted = false');
		$res = $stm->execute($user_id);
	} else {
		$stm = $db->prepare('SELECT count(id) FROM links WHERE public = true AND deleted = false');
		$res = $stm->execute();
	}
	unless ($res == 0E0) {
		$row = $stm->fetch();
		my $link_count = $row->[0];

		my $page = &real_page();

		unless($pub) {
			$stm = $db->prepare('SELECT id, title, description, url FROM links WHERE user_id = ? AND deleted = false ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?');
			$res = $stm->execute($user_id, $user_settings{links_per_page}, $page * $user_settings{links_per_page});
		} else {
			$stm = $db->prepare('SELECT id, title, description, url FROM links WHERE public = true AND deleted = false ORDER BY hits DESC, vdate DESC, url ASC LIMIT ? OFFSET ?');
			$res = $stm->execute($user_settings{links_per_page}, $page * $user_settings{links_per_page});
		}
		unless ($res == 0E0) {
			&print_head;

			&print_tagmap() if $user_settings{tagmap_on_top};

			print $cgi->start_div({-class => 'container'});

			unless ($pub) {
				&print_links(links => \$stm, link_count => $link_count, show_tags => 1, form_buttons => $cgi->submit(-name => 'delete', -value => 'Delete'));
			} else {
				&print_links(links => \$stm, link_count => $link_count, show_tags => 1);
			}

			print $cgi->end_div(); # div.container

			&print_tagmap() unless $user_settings{tagmap_on_top};

			&print_tail();
			&Exit();
		} else {
			push(@errors, 'Failed to retrieve links.'); # TODO improve message
		}
	} else {
		push(@errors, 'Failed to retrieve link count.'); # TODO improve message
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

sub action_add() { # {{{2
	my @errors;
	my ($url_title, $url_description, $url_public, $url_tags, $url, $url_alias, $url_r);

	$url				= $cgi->url_param('url');
	$url_description	= $cgi->url_param('description');
	$url_public			= 1;
	$url_tags			= '';
	$url_alias			= '';
	$url_title			= $cgi->url_param('title');
	$url_r				= $cgi->url_param('r');

	if ($cgi->param('submit')) { # check for errors if form was submited
		$url				= $cgi->param('url');
		$url_description	= $cgi->param('url_description');
		$url_public			= $cgi->param('url_public');
		$url_tags			= $cgi->param('url_tags');
		$url_title			= $cgi->param('url_title');
		$url_alias			= $cgi->param('url_alias');
		$url_r				= $cgi->param('url_r');

		push(@errors, 'No url') unless ($url);
		push(@errors, 'No url title') unless ($url_title);

		push(@errors, 'Tag may not have "#" character') if ($url_tags =~ m/#/);
		push(@errors, 'Tag may not have "/" character') if ($url_tags =~ m#/#);
		push(@errors, '"," is not allowed in tags') if ($url_tags =~ m#,#);
		push(@errors, '"<" and ">" are not allowed in tags') if ($url_tags =~ m#[<>]#);
		push(@errors, 'Tag may not have "%" character') if ($url_tags =~ m#%#);
		push(@errors, 'Alias may only contain a-z, A-Z, 0-9 and _') unless ($url_alias =~ m#^\w+$# or $url_alias eq '');
	}

	if ($#errors > -1 || !$cgi->param('submit')) { # Show form if it wasn't submitter yet, or if some fields had errors
		&print_head();
		&print_errors(@errors);

		&print_form_editurl(action => '/Add', submit => 'Add url', url_public => 1, url_r => $url_r);
		&print_tail();
		&Exit();
	}

	&add_url_tags_and_alias(url => $url, description => $url_description, title => $url_title, public => $url_public, tags => $url_tags, alias => $url_alias);

	if (defined $url_r and $url_r) {
		&redir_full($url);
	} else {
		&redir('/');
	}
	&Exit();
} # 2}}}

sub action_edit() { # {{{2
	my @errors;
	my ($stm, $res, $row);

	my $link_id = $cgi->param('id');
	my ($url, $url_title, $url_description, $url_tags, $url_public, $url_old_tags, $url_old_alias, $url_alias);
	my $invalid_url = 0;

	if ($cgi->param('submit')) { # check for errors if form was submited
		$url				= $cgi->param('url')								|| '';
		$url_title			= $cgi->param('url_title')							|| '';
		$url_description	= $cgi->param('url_description')					|| '';
		$url_tags			= lc(Encode::decode_utf8($cgi->param('url_tags')))	|| '';
		$url_old_tags		= $cgi->param('url_old_tags')						|| '';
		$link_id			= $cgi->param('link_id');
		$url_public			= $cgi->param('url_public')							|| 0;
		$url_alias			= lc(Encode::decode_utf8($cgi->param('url_alias')))	|| '';
		$url_old_alias		= $cgi->param('url_old_alias')						|| '';

		push(@errors, 'No URL') unless($url);
		push(@errors, 'No title') unless($url_title);

		push(@errors, 'Tag may not have "#" character') if ($url_tags =~ m/#/);
		push(@errors, 'Tag may not have "/" character') if ($url_tags =~ m#/#);
		push(@errors, '"," is not allowed in tags') if ($url_tags =~ m#,#);
		push(@errors, '"<" and ">" are not allowed in tags') if ($url_tags =~ m#[<>]#);
		push(@errors, 'Tag may not have "%" character') if ($url_tags =~ m#%#);
		push(@errors, 'Alias may only contain a-z, A-Z, 0-9 and _') unless ($url_alias =~ m#^\w+$# or $url_alias eq '');
	}

	if ($#errors > -1 || !$cgi->param('submit')) { # Show form if it wasn't submitter yet, or if some fields had errors
		if ($#errors == -1 && !$cgi->param('submit')) {
			$stm = $db->prepare('SELECT url, title, description, public FROM links WHERE user_id = ? AND id = ?');
			$res = $stm->execute($user_id, &short2dec($link_id));
			if ($res == 1) {
				$row = $stm->fetch();
				$url = $row->[0];
				$url_title = $row->[1];
				$url_description = $row->[2];
				$url_public = $row->[3];
				$url_tags = '';

				&unfilter_HTML_field(\$url_title, \$url_description);

				$stm = $db->prepare('SELECT tag FROM tagmap WHERE user_id = ? AND link_id = ?');
				$res = $stm->execute($user_id, &short2dec($link_id));
				if ($res > 0) {
					while ($row = $stm->fetch()) {
						$url_tags .= ' ' . $row->[0];
					}
					$url_tags =~ s/^ //;
				}
				$url_old_tags = $url_tags;

				$stm = $db->prepare('SELECT alias FROM aliases WHERE user_id = ? AND link_id = ? LIMIT 1');
				$res = $stm->execute($user_id, &short2dec($link_id));
				if ($res != 0E0 and $res > 0) {
					$row = $stm->fetch();
					$url_old_alias = $url_alias = $row->[0];
				}
			} else {
				$invalid_url = 1;
				push(@errors, 'Invalid URL. Goto ' . $cgi->a({-href => "$settings{base_dir}/"}, 'start'));
			}
		}

		&print_head();
		&print_errors(@errors);
		&print_form_editurl(url => $url, url_title => $url_title, url_description => $url_description, url_tags => $url_tags, url_old_tags => $url_old_tags, url_alias => $url_alias, url_old_alias => $url_old_alias, action => '/Edit', link_id => $link_id, submit => 'Update', url_public => $url_public) unless ($invalid_url);
		&print_tail();
		&Exit();
	}

	&filter_HTML_field(\$url_title, \$url_description, \$url_public, \$user_id, \$url_tags, \$url_old_tags);
	$db->do('UPDATE links SET url = ?, title = ?, description = ?, public = ? WHERE user_id = ? AND id = ?', undef, $url, $url_title, $url_description, $url_public, $user_id, &short2dec($link_id));

	if ($url_alias ne $url_old_alias) {
		$db->do('DELETE FROM aliases WHERE user_id = ? AND link_id = ?', undef, $user_id, &short2dec($link_id));
		&add_alias(alias => $url_alias, link_id => $link_id);
	}

	if ($url_tags ne $url_old_tags) {
		$url_tags =~ s#/# #g;
		$url_tags =~ s#  # #g;
		$url_tags =~ s#^ ##g;
		$url_tags =~ s# $##g;

		my @old_tags = split(/ /, $url_old_tags);
		my @new_tags = split(/ /, $url_tags);
		my (%old_t, %new_t);
		my (@add_tags, @rem_tags);

		@old_t{@old_tags} = undef;
		@new_t{@new_tags} = undef;

		@add_tags = grep {not exists $old_t{$_}} @new_tags;
		@rem_tags = grep {not exists $new_t{$_}} @old_tags;

		foreach my $tag (@add_tags) {
			&add_tag(tag => $tag, link_id => $link_id);
		}
		$stm = $db->prepare('DELETE FROM tagmap WHERE user_id = ? AND link_id = ? AND tag = ?');
		foreach my $tag (@rem_tags) {
			$stm->execute($user_id, &short2dec($link_id), $tag);
		}
	}

	&redir_full($cgi->param('referer'));
	&Exit();
} # 2}}}

sub action_redir() { # {{{2
	my @errors;
	my ($stm, $res, $row, $link_id);

	my $link = $cgi->url(-path_info => 1, -absolute => 1);

	if ($link =~ m#^$settings{base_dir}/R([a-zA-Z0-9]+)$#) {
		$link_id = "$1";

		unless($pub) {
			$db->do('UPDATE links SET hits = hits + 1, vdate = now() WHERE user_id = ? AND id = ?', undef, $user_id, &short2dec($link_id));
		}

		$stm = $db->prepare('SELECT url FROM links WHERE (user_id = ? OR public = true) AND id = ?');
		$res = $stm->execute($user_id, &short2dec($link_id));

	} elsif ($link =~ m#^$settings{base_dir}/A/(\w+)$# and ! $pub) {
		my $link_alias = "$1";

		$stm = $db->prepare('SELECT link_id FROM aliases WHERE user_id = ? and alias = ? LIMIT 1');
		$res = $stm->execute($user_id, $link_alias);
		if ($res != 0E0) {
			my $row = $stm->fetch();
			$link_id = $row->[0];
			$db->do('UPDATE links SET hits = hits + 1, vdate = now() WHERE user_id = ? AND id = ?', undef, $user_id, $link_id);
			$stm = $db->prepare('SELECT url FROM links WHERE user_id = ? AND id = ?');
			$res = $stm->execute($user_id, $link_id);
		} else {
			push (@errors, 'Invalid alias');
		}

	} else {
		push (@errors, 'Invalid redirect request');
	}


	if ($res != 0E0 and $#errors eq -1) {
		my $row = $stm->fetch();
		my $target_url = $row->[0];
		unless ($target_url =~ m#/R[A-Za-z0-9]+$#) {
			$target_url = 'http://' . $target_url unless ($target_url =~ m#^[a-zA-Z0-9]+://#); # FIXME: Do I need this?
			print $cgi->redirect($target_url);
			&Exit();
		} else {
			push(@errors, 'You tried crafted URL, smartass.')
		}
	} else {
		push(@errors, 'Invalid link');
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

sub action_import() { #{{{2
	my @errors = ();
	my $lightweight_fh;
	my ($stm, $res, $row);

	if ($cgi->param('submit')) {
		# Checks
		$lightweight_fh = $cgi->param('bookmark_file');
		my $type = $cgi->uploadInfo($lightweight_fh)->{'Content-Type'};
		push (@errors, 'Not html file') unless ($type eq 'text/html');
	}

	if ($#errors > -1 || !$cgi->param('submit')) {
		&print_head();

		&print_errors(@errors);

		print $cgi->start_multipart_form(-action => '/Import', -class => 'fancy', -id => 'import');
		print $cgi->start_fieldset(), $cgi->legend('Import bookmarks');
		print $cgi->label('Exported bookmark file (*.html)'), '<br/>';
		print $cgi->filefield(-class => 'w', -name => 'bookmark_file', -default => 'starting value', -maxlength => 256, -accept => 'text/html');
		print $cgi->end_fieldset();
		print $cgi->submit(-name => 'submit', -value => 'Upload');
		print $cgi->end_multipart_form();
		&print_tail();
		&Exit();
	}

	my $io_handle = $lightweight_fh->handle;
	my $buffer;
	my $filename = File::Temp::tempfile();

	open (OUTFILE, '>', "$settings{tmp_dir}/$filename");
	while ($io_handle->read($buffer, 1024)) {
		print OUTFILE $buffer;
	}
	close(OUTFILE);

	$stm = $db->prepare('INSERT INTO links (url, title, description, user_id) VALUES (?, ?, ?, ?)');
	my $imported = 0;

	open (INFILE, '<', "$settings{tmp_dir}/$filename");

	my ($line, $url, $title, $description) = ('', '', '', '');
	my $skip_read = 0;
	do {
		$line = <INFILE> unless $skip_read;
		$skip_read = 0;
		if ($line =~ m#<DT>#i) {
			($url, $title) = ($line =~ m#HREF="(.+?)".+?>(.+?)</A>#i);
			$line = <INFILE>;
			($description) = ($line =~ m#<DD>(.+?)</DD>#i);
			$skip_read++ unless ($description);

			if ($url && $title) {
				&filter_HTML_field(\$title, \$description);
				$res = $stm->execute($url, $title, $description, $user_id);
				$imported++ unless $res == 0E0;
				($url, $title, $description) = ('', '', '');
			}
		}
	} while ($line);
	close(INFILE);
	unlink "$settings{tmp_dir}/$filename";

	&print_head();
	print $cgi->h1('Contratulations');
	print $cgi->p("$imported unique bookmarks imported");
	&print_tail();
	&Exit();
} # 2}}}

sub action_not_faund() { # {{{2
	&print_head();
	print
		$cgi->h1('Page not found'),
		$cgi->p('Sorry, but ' . Encode::decode_utf8(URI::Encode::uri_decode($cgi->url(-path_info => 1, -absolute => 1) . ' was not found.'), 1)),
		$cgi->p('Go to', $cgi->a({-href => "$settings{host}$settings{base_dir}"}, 'start'));
	&print_tail();
	&Exit();
} # 2}}}

sub action_login() { # {{{2
	my ($stm, $res, $row);
	my $username = lc(Encode::decode_utf8($cgi->param('username')))	|| '';
	my $password = $cgi->param('password');
	$cgi->param(-name => 'password', -value => '');
	my $auth_failed = 0;
	my @errors;

	if ($cgi->param('submit')) {
		$stm = $db->prepare('SELECT password, id, settings FROM users WHERE username = ? AND active = true LIMIT 1');
		$res = $stm->execute($username);
		if ($res != 0E0) {
			$row = $stm->fetch();
			my $stored_password	= $row->[0];
			$user_id			= $row->[1];
			my $settings		= $row->[2];

			if ($stored_password eq Digest::MD5::md5_base64($password)) {
				$session->param('password', $stored_password);
				$session->param('user_id', $user_id);
				$session->param('settings', $settings);

				&redir('/');
				&Exit();
			} else {
				push(@errors, 'Invalid username or password');
			}
		} else {
			push(@errors, 'Invalid username or password');
		}
	}

	&print_head;
	&print_errors(@errors);
	print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/Login", -enctype => 'multipart/form-data', -id => 'login', -class => 'fancy');
	print $cgi->start_fieldset();
	print $cgi->label('Login:');
	print '<br/>';
	print $cgi->textfield(-class => 'w', -name => 'username', -value => '', -maxlength => $field_limits{username_max_len});
	print '<br/>';
	print $cgi->label('Password:');
	print '<br/>';
	print $cgi->password_field(-class => 'w', -name => 'password', -value => '');
	print $cgi->end_fieldset();
	print $cgi->submit(-name => 'submit', -value => 'Login');
	print $cgi->a({-href => "$settings{base_dir}/ChPass"}, 'Forgot password');
	print $cgi->a({-href => "$settings{base_dir}/Register"}, 'Register') if $settings{allow_register_new_users};
	print $cgi->end_form();


#	&print_screenshots(); TODO
	&print_supported_browsers();

	&print_tail();
	&Exit();
} # 2}}}

sub action_logout() { # {{{2
	$session->delete();
	$session->flush();
	$user_id = 0;
	&redir('/');
	&Edit();
} # 2}}}

sub action_register() { # {{{2
	my @errors;
	my @cctime = localtime;
	my $this_year = $cctime[5] + 1900;
	my ($stm, $res, $row);

	my ($username, $password, $password2, $email, $email2, $name, $surname, $day, $month, $year, $sex, $country, $city, $accept_license);

	# Check fields if form was submitted {{{3
	if ($cgi->param('submit')) {
		# checks
		$username		= lc(Encode::decode_utf8($cgi->param('username')))	|| '';
		$password		= $cgi->param('password') 		|| '';
		$password2		= $cgi->param('password2') 		|| '';
		$email			= lc(Encode::decode_utf8($cgi->param('email'))) 	|| '';
		$email2			= lc(Encode::decode_utf8($cgi->param('email2')))	|| '';
		$name			= $cgi->param('name') 			|| '';
		$surname		= $cgi->param('surname') 		|| '';
		$day			= $cgi->param('day') 			|| '';
		$month			= $cgi->param('month') 			|| '';
		$year			= $cgi->param('year') 			|| '';
		$sex			= $cgi->param('sex') 			|| '';
		$country		= $cgi->param('country') 		|| '';
		$city			= $cgi->param('city') 			|| '';
		$accept_license = $cgi->param('accept_license') || '';

		# Clear password fields. Password is saved in variable
		$cgi->param(-name => 'password', -value => '');
		$cgi->param(-name => 'password2', -value => '');
		$cgi->param(-name => 'accept_license', -value => '');

		if (length($password) < $settings{min_password_len}) {
			push(@errors, "Password too short. Enter at least $settings{min_password_len} characters");
			$password = $password2 = '';
			$cgi->param(-name => 'password', -value => '');
			$cgi->param(-name => 'password2', -value => '');
		}
		if ($password ne $password2) {
			push(@errors, "Passwords doesn't match");
			$password = $password2 = '';
			$cgi->param(-name => 'password', -value => '');
			$cgi->param(-name => 'password2', -value => '');
		}
		$password2 = '';

		# turn password into hash ASAP
		$password = Digest::MD5::md5_base64($password) if ($password ne '');

		if ($username ne '') {
			if ($username =~ m/[a-z0-9_-]+/i) {
				# delete expired requests
				$db->do("DELETE FROM users WHERE activated = false AND last_visit + interval '? hours' < now()", undef, $settings{request_timeout});
				$stm = $db->prepare('SELECT count(username) FROM users WHERE username = ? LIMIT 1');
				$stm->execute($username);
				$row = $stm->fetch();
				if ($row->[0]) {
					push(@errors, "Username '$username' already used.");
					$cgi->param(-name => 'username', -value => '');
				}
			} else {
				push(@errors, 'Username contains illegal characters. Valid characters are: a-z A-Z 0-9 _ -');
				$cgi->param(-name => 'username', -value => '');
			}
		} else {
			push(@errors, 'Username not entered');
			$cgi->param(-name => 'username', -value => '');
		}
		if ($email =~ m/^[\w\.\+\-=]+@[\w\.\-]+\.[\w\-]+$/) {
			# delete expired requests
			$stm = $db->prepare('SELECT count(email) FROM users WHERE email = ? LIMIT 1');
			$stm->execute($email);
			$row = $stm->fetch();
			if ($row->[0]) {
				push(@errors, "Account with email '$email' is already registered.");
				$cgi->param(-name => 'email', -value => '');
				$cgi->param(-name => 'email2', -value => '');
			}
		} else {
			push(@errors, 'Invalid e-mail');
			$email = $email2 = $password = $password2 = '';
			$cgi->param(-name => 'email', -value => '');
			$cgi->param(-name => 'email2', -value => '');
		}
		if ($email ne $email2) {
			push(@errors, "E-mails doesn't match");
			$email = $email2 = $password = $password2 = '';
			$cgi->param(-name => 'email', -value => '');
			$cgi->param(-name => 'email2', -value => '');
		}
		if (length($name) < 2) {
			push(@errors, 'Invalid name');
			$name = $password = $password2 = '';
			$cgi->param(-name => 'name', -value => '');
		}
		if (length($surname) < 2) {
			push(@errors, 'Invalid surname');
			$surname = $password = $password2 = '';
			$cgi->param(-name => 'surname', -value => '');
		}
		unless ($sex =~ m/[MF]/) {
			push(@errors, 'No sex');
			$sex = '?';
			$password = $password2 = '';
			$cgi->param(-name => 'sex', -value => '');
		}
		if ($day < 1 || $day > 31) {
			push(@errors, 'Invalid day of birth date');
			$day = 0;
			$password = $password2 = '';
			$cgi->param(-name => 'day', -value => '');
		}
		if ($month < 1 || $month > 12) {
			push(@errors, 'Invalid month of birth date');
			$month = 0;
			$password = $password2 = '';
			$cgi->param(-name => 'month', -value => '');
		}
		if ($year > $this_year || $year < $this_year - $settings{max_age_assumed}) {
			push(@errors, 'Invalid year of birth date');
			$year = 0;
			$password = $password2 = '';
			$cgi->param(-name => 'year', -value => '');
		}
		if ($year < $this_year - $settings{max_age_assumed}) {
			push(@errors, 'You are too young for this. Go out and play with your Friends');
			$year = 0;
			$password = $password2 = '';
			$cgi->param(-name => 'year', -value => '');
		}
		unless (Locale::Country::country2code($country)) {
			push(@errors, 'Invalid country');
			$country = $password = $password2 = '';
			$cgi->param(-name => 'country', -value => '');
		}
		if (length($city) < 3) {
			push(@errors, 'Invalid city');
			$city = $password = $password2 = '';
			$cgi->param(-name => 'city', -value => '');
		}
		if ($accept_license ne 'YES') {
			push(@errors, 'License not accepted');
			$accept_license = '';
			$cgi->param(-name => 'accept_license', -value => '');
		}
	}
	# 3}}}

	# Draw form if nessacery {{{3
	if ($#errors > -1 || !$cgi->param('submit')) { # Show form if it wasn't submitter yet, or if some fields had errors
		&print_head;

		&print_errors(@errors);

		# make really, really sure password fields are blank
		$cgi->param(-name => 'password', -value => '');
		$cgi->param(-name => 'password2', -value => '');

		print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/Register", -enctype => 'multipart/form-data', -id => 'register', -class => 'fancy');

		print $cgi->start_fieldset(), $cgi->legend('General info');

		print $cgi->label('Username', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'username', -maxlength => $field_limits{username_max_len}), '<br/>';

		print $cgi->label('e-mail', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'email', -maxlength => $field_limits{email_max_len}), '<br/>';
		print $cgi->label('e-mail again', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'email2', -maxlength => $field_limits{email_max_len}), '<br/>';

		print $cgi->label("Password (min. $settings{min_password_len} characters):", $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->password_field(-class => 'w', -name => 'password'), '<br/>';
		print $cgi->label('Password again', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->password_field(-class => 'w', -name => 'password2'), '<br/>';
		print $cgi->end_fieldset();

		print $cgi->start_fieldset(), $cgi->legend('Personal info');

		print $cgi->label('Name', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'name', -maxlength => $field_limits{name_max_len}), '<br/>';
		print $cgi->label('Surname', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'surname', -maxlength => $field_limits{surname_max_len}), '<br/>';

		print $cgi->label('Date of birth', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->label('Day: ');
		print $cgi->popup_menu(
			-name		=> 'day'
			,-values	=> [0 .. 31]
			,-labels	=> {0 => ''}
		);
		print $cgi->label(' Month: ');
		print $cgi->popup_menu(
			-name		=> 'month'
			,-values	=> [0 .. 12]
			,-labels	=> {
				0 => '', 1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
				7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'
			}
		);
		print $cgi->label(' Year: ');
		print $cgi->popup_menu(-name => 'year', -values => ['', sort { $b <=> $a } $this_year - $settings{max_age_assumed} .. $this_year]), '<br/>';

		print $cgi->label('Sex', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->popup_menu(
			 -name		=> 'sex'
			,-values	=> ['?', 'F', 'M']
			,-labels	=> {'?' => '', F => 'Female', M => 'Male'}
		), '<br/>';

		print $cgi->label('Country', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';

		my @countries = Locale::Country::all_country_names();
		unshift(@countries, '');
		print $cgi->popup_menu(
			 -name		=> 'country'
			,-values	=> \@countries
		), '<br/>';

		print $cgi->label('City', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
		print $cgi->textfield(-class => 'w', -name => 'city', -maxlength => $field_limits{city_max_len}), '<br/>';
		print $cgi->end_fieldset();

		print $cgi->start_fieldset(), $cgi->legend('License agreement');
		print $cgi->div('USE AT YOUR OWN RISK.<br/><b>TODO</b>'), '<br/>'; # FIXME
		print $cgi->checkbox(-name => 'accept_license', -value => 'YES', -label => 'Accept License', -checked => 0);
		print $cgi->end_fieldset();

		print $cgi->submit(-name => 'submit', -value => 'Register');

		print $cgi->end_form();
		&print_tail();
		&Exit();
	}
	# 3}}}

	# double check that all fields are html free
	&filter_HTML_field(\$username, \$email, \$email2, \$name, \$surname, \$day, \$month, \$year, \$sex, \$city, \$accept_license);

	# register new user {{{3
	$stm = $db->prepare('SELECT count(username) FROM users WHERE username = ? LIMIT 1');
	$stm->execute($username);
	$row = $stm->fetch();
	if ($row->[0]) {
		push(@errors, "Username '$username' already used. Pick another username") if ($row->[0]);
		$cgi->param(-name => 'username', -value => '');
		&action_register();
	}

	my $bdate = "$year-$month-$day";
	my $user_id;
	my $confirm_key = Digest::MD5::md5_hex(rand() . $password . rand() . $username . rand() . $email . rand());
	$res = $db->do('INSERT INTO users (username, password, email, name, surname, bdate, sex, city, country) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', undef, $username, $password, $email, $name, $surname, $bdate, $sex, $city, Locale::Country::country2code($country));
	if ($res != 0E0) {
		$stm = $db->prepare('SELECT id FROM users WHERE username = ? LIMIT 1');
		$stm->execute($username);
		$row = $stm->fetch();
		$user_id = $row->[0];

		$res = $db->do("INSERT INTO user_requests (user_id, request, confirm_key) VALUES (?, 'register', ?)", undef, $user_id, $confirm_key);
		if ($res != 0E0) {
			$stm = $db->prepare("SELECT id FROM user_requests WHERE user_id = ? AND request = 'register' AND confirm_key = ? LIMIT 1");
			$res = $stm->execute($user_id, $confirm_key);
			if ($res != 0E0) {
				$row = $stm->fetch();
				my $request_id = $row->[0];
				my $email_msg = Email::Simple->create(
					header => [
						To      => "\"$name $surname\" <$email>",
						From    => "<$settings{email_from}>",
						Subject => "Confirm your registration at $settings{host}$settings{basedir}",
					]
					,body => <<EOF
Hello, $name $surname

You have been registered at $settings{host}$settings{basedir}
In order to finish your registeration, you need to visit this link below
within 48 hours since registering.
$settings{host}$settings{basedir}/Confirm?action=register&id=$request_id&key=$confirm_key

If you didn't register and/or received this e-mail by accident, please
fallow link below
$settings{host}$settings{basedir}/Confirm?action=register&id=$request_id&key=$confirm_key&cancel=1
EOF
				);
				sendmail($email_msg);

				&print_message_and_redir(title => 'Registeration successful', message => 'Confirmation email was sent to you. In order to finish registeration you need to visit link in email withing 48 hours from now.<br/>Make sure to check spam folder especially if you use gmail.', timeout => 15, target => '/');
			} else {
				push(@errors, 'Registration failed 3'); # FIXME
			}
		} else {
			push(@errors, 'Registration failed 2'); # FIXME
		}
	} else {
		push(@errors, 'Registration failed 1'); # FIXME
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();

	# 2}}}

} # 2}}}

sub action_confirm() { # {{{2
	my $action			= $cgi->url_param('action')	|| '';
	my $request_id		= $cgi->url_param('id')	   	|| 0;
	my $confirm_key 	= $cgi->url_param('key')   	|| '';
	my $cancel			= $cgi->url_param('cancel')	|| $cgi->param('cancel') || 0;

	my $user_id;
	my @errors;
	my ($stm, $res, $row);

	my $data = &validate_request(request => $action, request_id => $request_id, confirm_key => $confirm_key);



	if (defined $data) {
		$user_id = ${$data}{user_id};
		if ($user_id > 0) {

			if ($cancel) { # cancel request
				if ($action eq 'register') {
					# this will delete user request as well
					$db->do("DELETE FROM users WHERE id = (SELECT user_id FROM user_requests WHERE request = ? AND id = ? and confirm_key = ?", undef, $action, $request_id, $confirm_key);
				} else {
					$db->do("DELETE FROM user_requests WHERE request = ? AND id = ? and confirm_key = ?", undef, $action, $request_id, $confirm_key);
				}
				&print_message_and_redir(title => 'Requests canceled', message => 'Request was cancelled', timeout => 3, target => '/');
			}



			# register {{{3
			if ($action eq 'register') {
				$res = $db->do('UPDATE users SET activated = true, active = true, reg_ip = ?, last_ip = ? WHERE id = ?', undef, $remote_addr, $remote_addr, $user_id);
				if ($res != 0E0) {
					$db->do("DELETE FROM user_requests WHERE user_id = ? AND request = 'register'", undef, $user_id);

					&print_message_and_redir(title => 'Congratulations', message => 'Your account is activated', target => '/', timeout => 5);

				} else {
					push(@errors, 'Failed to activate account');
				}
			}
			# 3}}}


			# chpass {{{3
			elsif ($action eq 'chpass') {
				my ($password, $password2);

				if ($cgi->param('submit')) {
					$password	= $cgi->param('password') 	|| '';
					$password2	= $cgi->param('password2') 	|| '';

					if (length($password) < $settings{min_password_len}) {
						push(@errors, "Password too short. Enter at least $settings{min_password_len} characters");
						$password = $password2 = '';
						$cgi->param(-name => 'password', -value => '');
						$cgi->param(-name => 'password2', -value => '');
					}
					if ($password ne $password2) {
						push(@errors, "Passwords doesn't match");
						$password = $password2 = '';
						$cgi->param(-name => 'password', -value => '');
						$cgi->param(-name => 'password2', -value => '');
					}
					$password2 = '';
				}

				if ($#errors == -1 && $cgi->param('submit')) {
					$password = Digest::MD5::md5_base64($password);
					$res = $db->do('UPDATE users SET password = ? WHERE id = ?', undef, $password, $user_id);
					if ($res != 0E0) {
						$db->do("DELETE FROM user_requests WHERE user_id = ? AND request = 'chpass'", undef, $password, $user_id);
						&print_message_and_redir(title => 'Password updated', message => 'You may now uses your new password.', timeout => 3, target => '/');
					} else {
						push(@errors, 'Failed to updated password');
					}
				}

				&print_head();
				&print_errors(@errors);
				my $request_url = $cgi->url(-absolute => 1, -query => 1, -path_info => 1);
				$request_url =~ s/;/\&/g;
				print $cgi->start_form(-method => 'POST', -action => $request_url, -enctype => 'multipart/form-data', -id => 'chpass', -class => 'fancy');

				print $cgi->start_fieldset();
				print $cgi->label("Password (min. $settings{min_password_len} characters)", $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
				print $cgi->password_field(-class => 'w', -name => 'password'), '<br/>';
				print $cgi->label('Password again', $cgi->sup({-class => 'mandatory'}, '*')), '<br/>';
				print $cgi->password_field(-class => 'w', -name => 'password2'), '<br/>';
				print $cgi->end_fieldset();
				print $cgi->submit(-name => 'submit', -value => 'Change Password');
				print $cgi->submit(-name => 'cancel', -value => 'Cancel');
				print $cgi->end_form();
				&print_tail();
				&Exit();
			}
			# 3}}}


			# chemail {{{3
			elsif ($action eq 'chemail') {
				my $request_id;

				if ($cgi->param('submit')) {
					my $confirm_key = Digest::MD5::md5_hex(rand() . time(). rand());

					$stm = $db->prepare("SELECT name, surname FROM users WHERE id = ?");
					$res = $stm->execute($user_id);
					if ($res != 0E0) {
						$row = $stm->fetch();
						my $name	= $row->[0];
						my $surname	= $row->[1];

						$res = $db->do("INSERT INTO user_requests (user_id, request, confirm_key, data) VALUES (?, 'chemail2', ?, ?)", undef, $user_id, $confirm_key, ${$data}{data});
						if ($res != 0E0) {

							$stm = $db->prepare("SELECT id FROM user_requests WHERE user_id = ? AND request = 'chemail2' AND confirm_key = ?");
							$res = $stm->execute($user_id, $confirm_key);
							if ($res != 0E0) {
								$row = $stm->fetch();
								$request_id = $row->[0];

								$res = $db->do("DELETE FROM user_requests WHERE user_id = ? AND request = 'chemail'", undef, $user_id);
								if ($res != 0E0) {
									my $email_msg = Email::Simple->create(
										header => [
											To      => "\"$name $surname\" <${$data}{data}>",
											From    => "<$settings{email_from}>",
											Subject => 'e-mail change requested',
										]
										,body => <<EOF
Hello, $name $surname

You have requested to change your email
In order to verify your new e-mail address visit link below within 48 hours since registering.
$settings{host}$settings{basedir}/Confirm?action=chemail2&id=$request_id&key=$confirm_key

If you didn't request to change email and/or received this e-mail by accident,
please fallow link below
$settings{host}$settings{basedir}/Confirm?action=chemail2&id=$request_id&key=$confirm_key&cancel=1
EOF
									);
									sendmail($email_msg);

									&print_message_and_redir(title => 'Verification e-mail sent', message => 'Verification email was sent to you. In order to change your e-mail you need to visit link in email withing 48 hours from now.', timeout => 10, target => '/');
								} else {
									push(@errors, 'Unexpected error 1');
								}
							} else {
								push(@errors, 'Invalid confirmation key');
							}
						} else {
							push(@errors, 'Failed to make new request');
						}
					} else {
						push(@errors, 'Invalid user');
					}

					&print_head();
					&print_errors(@errors);
					&print_tail();
					&Exit();
				}

				&print_head();
				&print_errors(@errors);
				my $request_url = $cgi->url(-absolute => 1, -query => 1, -path_info => 1);
				$request_url =~ s/;/\&/g;
				print $cgi->start_form(-method => 'POST', -action => $request_url, -enctype => 'multipart/form-data', -id => 'chemail', -class => 'fancy');
				print $cgi->start_fieldset();
				print $cgi->legend('Changing email address');

				print $cgi->label('New e-mail address'), '<br/>';
				print $cgi->textfield(-class => 'w', -name => 'new_email', -maxlength => $field_limits{email_max_len}, -default => ${$data}{data}, -readonly => 'readonly');

				print $cgi->end_fieldset();
				print $cgi->submit(-name => 'submit', -value => 'Continue changing email');
				print $cgi->submit(-name => 'cancel', -value => 'Cancel');
				print $cgi->end_form();
				&print_tail();
				&Exit();
			}
			# 3}}}


			# chemail2 {{{3
			elsif ($action eq 'chemail2') {
				$res = $db->do("DELETE FROM user_requests WHERE user_id = ? AND request = 'chemail2'", undef, $user_id);

				$res = $db->do('UPDATE users SET email = ? WHERE id = ?', undef, ${$data}{data}, $user_id);
				if ($res != 0E0) {
					&print_message_and_redir(title => 'E-mail address changed', message => 'Your email address was updated succesfuly', timeout => 3, target => '/');
				} else {
					&print_message_and_redir(title => 'Failed to update e-mail address', message => 'Failed to update your e-mail address.', timeout => 5, target => '/');
				}
			}
			# 3}}}


			# deleteaccount {{{3
			elsif ($action eq 'deleteaccount') {
				my ($password, $password2, $name, $surname, $email);

				if ($cgi->param('submit')) {
					$password = Digest::MD5::md5_base64($cgi->param('password'));
					$password2 = Digest::MD5::md5_base64($cgi->param('password2'));
					$cgi->param('password', '');
					$cgi->param('password2', '');
					if ($password ne $password2) {
						push(@errors, 'Invalid password');
					} else {
						$password2 = '';
						$stm = $db->prepare('SELECT password, name, surname, email FROM users WHERE id = ?');
						$res = $stm->execute($user_id);
						if ($res != 0E0) {
							$row = $stm->fetch();

							$password2	= $row->[0];
							$name		= $row->[1];
							$surname	= $row->[2];
							$email		= $row->[3];

							push(@errors, 'Invalid password') if ($password ne $password2);

						} else {
							push(@errors, 'Invalid password');
						}

					}
					$password = $password2 = '';
				}

				if ($#errors == -1 && $cgi->param('submit')) {
					$res = $db->do('DELETE FROM users WHERE id = ?', undef, $user_id);
					if ($res != 0E0) {
						&print_message_and_redir(title => 'Account deleted', message => 'Your account and all data were deleted.', timeout => 5, target => '/');
					} else {
						push(@errors, 'Failed to delete account');
					}
				}

				&print_head();
				&print_errors(@errors);
				my $request_url = $cgi->url(-absolute => 1, -query => 1, -path_info => 1);
				$request_url =~ s/;/\&/g;
				print $cgi->start_form(-method => 'POST', -action => $request_url, -enctype => 'multipart/form-data', -id => 'deleteaccount', -class => 'fancy');
				print $cgi->start_fieldset();
				print $cgi->legend('Delete account');

				print $cgi->label('Password'), '<br/>';
				print $cgi->password_field(-class => 'w', -name => 'password');
				print $cgi->label('Password again'), '<br/>';
				print $cgi->password_field(-class => 'w', -name => 'password2');

				print $cgi->end_fieldset();
				print $cgi->submit(-name => 'submit', -value => 'DELETE ACCOUNT');
				print $cgi->submit(-name => 'cancel', -value => 'Cancel');
				print $cgi->end_form();
				&print_tail();
				&Exit();
			}
			# 3}}}

			else {
				push(@errors, 'Unsupported request');
			}
		} else {
			push(@errors, 'Invalid user.');
		}
	} else {
		push(@errors, 'Invalid request.');
	}

	&print_head();
	&print_errors(@errors);
	&print_tail();
	&Exit();
} # 2}}}

# 1}}}

# Misc {{{1

sub dec2short() { # {{{2
	my $dec = $_[0];
	my $short = '';
	while ($dec > 0) {
		$short .= $dec2short_table[$dec % $short_base_n];
		$dec = POSIX::floor($dec / $short_base_n);
	}
	return $short;
} # 2}}}

sub short2dec() { # {{{2
	my $short = $_[0];
	my $dec = 0;
	my $short_len = length($short);
	for (my $i = $short_len; $i > 0; $i--) {
		$dec = $dec * $short_base_n + $short2dec_table{substr($short, $i-1, 1)};
	}
	return $dec;
} # 2}}}

sub validate_request() { # {{{2
	my %param = @_;
	my ($stm, $res, $row);
	my %return_data;

	# delete expired registration requests and users
	#$db->do("DELETE FROM users WHERE activated = false AND reg_date + INTERVAL ? < now()", undef, "$settings{request_timeout} hours");
	$db->do("DELETE FROM users WHERE activated = false AND reg_date + INTERVAL '$settings{request_timeout} hours' < now()");

	# delete all other expired requests
	#$db->do("DELETE FROM user_requests WHERE req_date + ? < now()", undef, "$settings{request_timeout} hours");
	$db->do("DELETE FROM user_requests WHERE req_date + INTERVAL '$settings{request_timeout} hours' < now()");

	#$res = $db->do("SELECT user_id, data FROM user_requests WHERE request = ? AND id = ? AND confirm_key = ?", undef, $param{request}, $param{request_id}, $param{confirm_key});
	$stm = $db->prepare("SELECT user_id, data FROM user_requests WHERE request = ? AND id = ? AND confirm_key = ?");
	$res = $stm->execute($param{request}, $param{request_id}, $param{confirm_key});

	return undef if ($res == 0E0);

	$row = $stm->fetch();

	$return_data{user_id} = $row->[0];
	$return_data{data} = $row->[1];

	return \%return_data;
} # 2}}}

sub add_url() { # {{{2
	# returns undef on errors
	# returns url idzc
	my %param = @_;
	my $url				= exists $param{url}			? $param{url}			: '';
	my $url_title		= exists $param{title}			? $param{title}			: '';
	my $url_description	= exists $param{description}	? $param{description}	: '';
	my $url_public		= exists $param{public}			? $param{public}		: 'false';

	return undef unless($user_id);
	return undef unless($url);
	return undef unless($url_title);

	my ($stm, $res, $row);

	&filter_HTML_field(\$url_title, \$url_description, \$url_public);
	$res = $db->do('INSERT INTO links (user_id, url, title, description, public) VALUES (?, ?, ?, ?, ?)', undef, $user_id, $url, $url_title, $url_description, $url_public);
#	return undef if ($res == 0E0); #FIXME

	$stm = $db->prepare('SELECT id FROM links WHERE user_id = ? AND url = ? LIMIT 1');
	$res = $stm->execute($user_id, $url);
	return undef if ($res == 0E0);
	return undef if ($res == 0);
	$row = $stm->fetch();
	return &dec2short($row->[0]);
} # 2}}}

sub add_tag() { # {{{2
	# returns undef on errors
	my %param = @_;
	my $tag = lc(Encode::decode_utf8($param{tag}));
	my $link_id = $param{link_id};

	return undef unless($tag);

	&filter_HTML_field(\$tag);
	my $res = $db->do('INSERT INTO tagmap (user_id, tag, link_id) VALUES (?, ?, ?)', undef, $user_id, $tag, &short2dec($link_id));
#	return undef if ($res == 0E0);	# FIXME

	return 0;
} # 2}}}

sub add_alias() { # {{{2
	# returns undef on errors
	my %param = @_;
	my $alias = lc(Encode::decode_utf8($param{alias}));
	my $link_id = $param{link_id};

	return undef unless($alias);

	&filter_HTML_field(\$alias);

	$db->do('DELETE FROM aliases WHERE user_id = ? AND alias = ?', undef, $user_id, $alias); # Delete old alias
	my $res = $db->do('INSERT INTO aliases (user_id, alias, link_id) VALUES (?, ?, ?)', undef, $user_id, $alias, &short2dec($link_id));
#	return undef if ($res == 0E0);	# FIXME

	return 0;
} # 2}}}

sub real_page() { # {{{2
	my $page = $cgi->param('page') || 1;
	$page--;
	$page = 0 if $page < 0;
	$page = 0 unless $page =~ m/\d+/;
	return $page;
} # 2}}}

sub add_url_tags_and_alias() { # {{{2
	# returns undef on errors
	# otherwise returns 0
	my %param = @_;

	my $url = $param{url};
	my $url_title = $param{title};
	my $url_description = $param{description};
	my $url_tags = $param{tags};
	my $url_public = $param{public};
	my $url_alias = $param{alias};

	$url_tags =~ s#/# #g;
	$url_tags =~ s#  # #g;
	$url_tags =~ s#^ ##g;
	$url_tags =~ s# $##g;
	my @tags = split(/ /, $url_tags);

	my $link_id = &add_url(url => $url, title => $url_title, description => $url_description, public => $url_public);
	return undef unless defined($link_id);

	foreach my $tag (@tags) {
		&add_tag(tag => $tag, link_id => $link_id);
	}

	&add_alias(alias => $url_alias, link_id => $link_id);

	return 0;
} # 2}}}

sub page_numbers() { # {{{2
	# parameters: pages
	my $links = shift;
	my $pages = POSIX::ceil($links / $user_settings{links_per_page});
	my $html_code = '';

	my $show_pages = 5; # show x pages before and after current page

	my $page = $cgi->param('page') || 0;
	$page--;
	$page = 0 if ($page < 0);
	$page = 0 unless ($page =~ m/\d+/); # make sure it's a number
	my $from_page = $page - $show_pages + 1;
	my $to_page = $page + $show_pages + 2;

	$to_page += -$from_page + 1 if $from_page < 1;
	$from_page -= ($to_page - $pages) if $to_page > $pages;

	$from_page = 1 if $from_page < 1;
	$to_page = $pages + 1 if $to_page > $pages;


	my $query = $cgi->url(-absolute => 1, -query => 1, -path_info => 1);
	$query =~ s/;/&/g;
	URI::Encode::uri_decode($query, 1);
	if ($query =~ m"[?&]page=\d+") {
		$query =~ s"([?&]page)=\d+"$1=%u";
	} else {
		$query .= $query =~ m/\?/ ? '&page=%u' : '?page=%u';
	}

	$html_code = $cgi->start_div({-class => 'pages'});

	$html_code .= $cgi->a({-href => sprintf($query, 1)}, 'First') if $from_page != 1;
	for(my $i = $from_page; $i < $to_page; $i++) {
		$html_code .= ($i != $page + 1) ? $cgi->a({-href => sprintf($query, $i)}, $i) : $cgi->div({-class => 'current_page'}, $i);
	}
	$html_code .= $cgi->a({-href => sprintf($query, $pages)}, 'Last') if $to_page != $pages + 1;
	$html_code .= $cgi->div({-class => 'clear'}, ''); # Important!!!

	$html_code .= $cgi->end_div(); # div.page
	return $html_code;
} # 2}}}

sub filter_HTML_field() { # {{{2
	# Pass array of references to scalars to filter
	#
	# http://www.escapecodes.info/
	foreach my $item (@_) {
		$$item =~ s/^ //g;
		$$item =~ s/ $//g;
		$$item =~ s/[ ]+/ /g;
		$$item =~ s/&/&amp;/g;
		$$item =~ s/</&lt;/g;
		$$item =~ s/>/&gt;/g;
		$$item =~ s/"/&quot;/g;
		$$item =~ s/'/&rsquo;/g;
	}
} # 2}}}

sub unfilter_HTML_field() { # {{{2
	# Pass array of references to scalars to filter
	#
	# http://www.escapecodes.info/

	foreach my $item (@_) {
		$$item =~ s/[ ]+/ /g;
		$$item =~ s/&amp;/&/g;
		$$item =~ s/&lt;/</g;
		$$item =~ s/&gt;/>/g;
		$$item =~ s/&quot;/"/g;
		$$item =~ s/&rsquo;/'/g;
	}
} # 2}}}

sub Exit() { # {{{2
#	$stm->finish(); # TODO FIXME
	$db->disconnect();
	exit;
} # 2}}}

sub redir() { # {{{2
	print $cgi->redirect("$settings{host}$settings{base_dir}$_[0]");
} # 2}}}

sub redir_full() { # {{{2
	if (defined($_[0]) && $_[0]) {
		print $cgi->redirect($_[0]);
	} else {
		&redir("$settings{host}$settings{base_dir}");
	}
} # 2}}}

# 1}}}

# print {{{1

sub print_search_plugin() { # {{{2
	# https://developer.mozilla.org/en/Creating_OpenSearch_plugins_for_Firefox
	print <<EOF
Content-Type: application/xml; charset=UTF-8

<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">
<ShortName>Linx search</ShortName>
<Description>Search bookmarks on Linx</Description>
<InputEncoding>UTF-8</InputEncoding>
<Image height="16" width="16" type="image/x-icon">$settings{host}$settings{base_dir}/gfx/favicon.png</Image>
<Url type="text/html" method="POST" template="$settings{host}$settings{base_dir}/Search">
	<Param name="search" value="{searchTerms}"/>
</Url>
</OpenSearchDescription>
EOF
;
	&Exit();
} # 2}}}

sub print_links() { # {{{2
	# \links link_count
	my %param = @_;
	my $links = $param{link_count};
	my $show_tags = $param{show_tags} || 0;
	my $pages = POSIX::ceil($links / $user_settings{links_per_page});
	my ($stm, $res, $row);
	$stm = ${$param{links}};

	my $page = $cgi->param('page') || 1;
	$page--;
	$page = 0 if $page < 0;
	$page = 0 unless $page =~ m/\d+/;

	my $tag_stm;


	if ($show_tags) {
		unless ($pub) {
			if ($user_settings{order_tags}) {
				$tag_stm = $db->prepare('SELECT tag FROM tagmap WHERE user_id = ? AND link_id = ? ORDER BY tag');
			} else {
				$tag_stm = $db->prepare('SELECT tag FROM tagmap WHERE user_id = ? AND link_id = ?');
			}
		} else {
			if ($settings{order_tags}) {
				$tag_stm = $db->prepare('SELECT tag FROM tagmap WHERE link_id = ? GROUP BY tag ORDER BY tag');
			} else {
				$tag_stm = $db->prepare('SELECT tag FROM tagmap WHERE link_id = ? GROUP BY tag');
			}
		}
	}

	print $cgi->start_div({-class => 'content', -id => 'content'});
	my $pn = &page_numbers($links);
	print $pn;

	my $tag_res;
	my $tag_row;
	my $i = 0;
	my $row_class = 'r0';

	my $JS_array_elements = '';

	if (exists $param{form_buttons}) {
		my $request_url = $cgi->url(-absolute => 1, -path_info => 1, -query => 1);
		$request_url =~ s/;/\&/g;

		print $cgi->start_form(-method => 'POST', -action => $request_url, -enctype => 'multipart/form-data', -class => 'clean');
		print $cgi->button(-value => 'Select all', -onClick => "var i;for(i=0;i<u.length;i++){document.getElementById(\"l\"+u[i]).checked=true;}");
		print $cgi->button(-value => 'Unselect all', -onClick => "var i;for(i=0;i<u.length;i++){document.getElementById(\"l\"+u[i]).checked=false;}");
		print $param{form_buttons};
	}

	print $cgi->start_div({-id => 'links'});
	while ($row = $stm->fetch()) {
		my $link_id			= &dec2short($row->[0]);
		my $url_title		= $row->[1];
		my $url_description	= $row->[2];
		my $url_real		= $row->[3];
		my $url				= "$settings{base_dir}/R$link_id";
		my $editurl			= "$settings{base_dir}/Edit?id=$link_id";
		my $deleteurl		= "$settings{base_dir}/Delete?id=$link_id";

		$url_description =~ s#\n\r#<br/>#g;
		$url_description =~ s#\r\n#<br/>#g;
		$url_description =~ s#\n#<br/>#g;
		$url_description =~ s#\r#<br/>#g;


#		print $cgi->start_div({-class => $row_class, -id => "u$i", -onClick => "r($link_id)"});
		print $cgi->start_div({-class => $row_class, -id => "u$i"});

		if (exists $param{form_buttons}) {
			print $cgi->checkbox(-name => "l$link_id", -id => "l$link_id", -checked => 0, -value => 1, -label => '');
			$JS_array_elements .= ",'$link_id'";
		}

		print $cgi->a({-class => 'eu', -href => $editurl}, '[edit]') if ($user_id && !$pub);
		print $cgi->a({-class => 't', -href => $url}, $url_title);

		if ($user_settings{show_url}) {
			print '<br/>';
			print $cgi->a({-class => 'ru', -href => $url}, $url_real);
		}

		print $cgi->start_div({-id => "d$i", -class => 'd'});
		if ($url_description) {
			print $url_description;
			print '<hr/>';
		}
		print $url_real;
		print $cgi->end_div(); # div#d...

		if ($show_tags) {
			unless($pub) {
				$tag_res = $tag_stm->execute($user_id, &short2dec($link_id));
			} else {
				$tag_res = $tag_stm->execute(&short2dec($link_id));
			}
			if ($tag_res != 0E0) { # tags
				print '<br/>';
				print $cgi->div({-class => 't'}, 'Tags:');
				while ($tag_row = $tag_stm->fetch()) {
					print ' '; # important!!!
					print $cgi->a({-class => 'tag', -href => "$settings{base_dir}/$tag_row->[0]"}, $tag_row->[0]);
				}
			}
		}
		print $cgi->end_div(); # div.r[01]#u...

		$row_class = $row_class eq 'r0' ? 'r1' : 'r0';
		$i++;
	}
	print $cgi->end_div(); # div#links

	if (exists $param{form_buttons}) {
		print $cgi->button(-value => 'Select all', -onClick => "var i;for(i=0;i<u.length;i++){document.getElementById(\"l\"+u[i]).checked=true;}");
		print $cgi->button(-value => 'Unselect all', -onClick => "var i;for(i=0;i<u.length;i++){document.getElementById(\"l\"+u[i]).checked=false;}");
		print $param{form_buttons};
		$JS_array_elements =~ s/^,//;
		print $cgi->script(
			{type => 'text/javascript'}
			,"var u=new Array($JS_array_elements);"
		);
		print $cgi->end_form();
	}

	print $pn;
	print $cgi->end_div(); # div.content
} # 2}}}

sub print_form_editurl() { # {{{2
	my %param = @_;
	print $cgi->start_form(-method => 'POST', -action => $settings{base_dir} . $param{action}, -enctype => 'multipart/form-data', -id => 'urledit', -class => 'fancy');
	print $cgi->start_fieldset();
	print $cgi->hidden(-name => 'link_id', -value => $param{link_id}) if exists $param{link_id};
	print $cgi->hidden(-name => 'url_old_tags', -value => $param{url_tags}) if exists $param{url_tags};
	print $cgi->hidden(-name => 'url_old_alias', -value => $param{url_alias}) if exists $param{url_alias};
	print $cgi->hidden(-name => 'referer', -value => $ENV{'HTTP_REFERER'}) if defined $ENV{'HTTP_REFERER'};
	print $cgi->hidden(-name => 'url_r', -value => $param{url_r}) if exists $param{url_r};
	print $cgi->label('Title');
	print '<br/>';
	print $cgi->textfield(-class => 'w', -name => 'url_title', -value => $param{url_title}, -maxlength => $field_limits{url_title_max_len});
	print '<br/>';
	print $cgi->label('url');
	print '<br/>';
	print $cgi->textfield(-class => 'w', -name => 'url', -value => $param{url}, -maxlength => $field_limits{tag_description_max_len});
	print '<br/>';
	print $cgi->label('Description');
	print '<br/>';
	print $cgi->textarea(-class => 'w', -name => 'url_description', -value => $param{url_description}, -rows => 10);
	print '<br/>';
	print $cgi->checkbox(-name => 'url_public', -checked => $param{url_public}, -value => 1, -label => 'Public');
	print '<br/>';
	print $cgi->label('Tags');
	print '<br/>';
	print $cgi->textfield(-class => 'w', -name => 'url_tags', -value => $param{url_tags});
	print '<br/>';
	print $cgi->label('Alias (will overwrite existing alias)');
	print '<br/>';
	print $cgi->textfield(-class => 'w', -name => 'url_alias',  -maxlength => $field_limits{alias_max_len}, -value => $param{url_alias});
	print $cgi->end_fieldset();

	print $cgi->submit(-name => 'submit', -value => exists $param{submit} ? $param{submit} : 'OK');
	print $cgi->end_form();
} # 2}}}

sub print_message_and_redir() { # {{{2
	# parameters: timeout, target, title, message, abs_path
	my %param = @_;

	if (exists $param{abs_path}) {
		&print_head(target => "$param{target}", timeout => $param{timeout});
	} else {
		&print_head(target => "$settings{base_dir}$param{target}", timeout => $param{timeout});
	}

	print $cgi->h1($param{title});
	print $cgi->p($param{message});
	print $cgi->p("You'll be redirected in $param{timeout} seconds");
	&print_tail();
	&Exit();
} # 2}}}

sub print_errors() { # {{{2
	if (@_) {
		print $cgi->start_div({-id => 'err'});
		foreach my $error (@_) {
			print $cgi->p($error);
		}
		print $cgi->end_div();
	}
} # 2}}}

sub print_head() { # {{{2
	my %param = @_;
	my $target	= $param{target}	|| '';
	my $timeout	= $param{timeout}	|| 5;

	my ($core_css, $theme_css);

	unless ($settings{devel}) {
		$core_css	= (-e "css/core.css") ? "css/core.css" : "css/core-devel.css";
		$theme_css	= (-e "themes/$user_settings{theme}/theme.css")	? "themes/$user_settings{theme}/theme.css" : "themes/$settings{theme}/theme-devel.css";

		$theme_css = "themes/$settings{fallback_themes}/theme.css" unless (-e "themes/$settings{fallback_themes}/theme.css");
	} else {
		$theme_css = "themes/$user_settings{theme}/theme-devel.css";
	}

	$theme_css = "themes/$settings{fallback_theme}/theme-devel.css" unless (-e $theme_css);
	$core_css = "css/core-devel.css" unless ($core_css && -e $core_css);


	print $cgi->header(-charset => 'UTF-8', -cookie => $session->cookie());
	print $cgi->start_html(
		-title => $settings{title}
		,-meta => {
			 Author			=> $linx{author}
			,Copyright		=> $linx{copyright}
			,Keywords		=> $settings{keywords}
			,Description	=> 'Store your links online'
		}
		,-head => [
			 $cgi->meta({-http_equiv => 'pragma',			-content => 'no-cache'})
			,$cgi->meta({-http_equiv => 'cache-control',	-content => 'no-cache'})
			,$cgi->meta({-http_equiv => 'expires',			-content => '-1'})
			,$cgi->meta({-http_equiv => 'Content-Type',		-content => 'text/html; charset=UTF-8'})

			,$cgi->Link({-rel => 'shortcut icon', -href => "$settings{base_dir}/gfx/favicon.png", -type => 'image/png'})
			,$cgi->Link({-rel => 'search', -href => "$settings{base_dir}/SearchPlugin", -type => 'application/opensearchdescription+xml', -title => 'linx search'})
			,$target ? $cgi->meta({-http_equiv => 'refresh',  -content => "$timeout;$target"}) : ''
		]
		,-lang			=> 'lv_LV'
		,-encoding		=> 'UTF-8'
		,-declare_xml	=> 'yes'
		,-style			=> ["$settings{base_dir}/$core_css", "$settings{base_dir}/$theme_css"]
#		,-script => {-language => 'text/javascript', -src => "$settings{base_dir}/dojo-release-1.5.0/dojo/dojo.js"}
	);
	print $cgi->start_div({-id => 'outer'});

	print $cgi->div(
		{-style => 'display:none'}
		,$cgi->comment('Do not send email to this address!!!')
		,'If you want to send spam, send it to'
		,$cgi->a(
			{-href => "mailto:$settings{spamtrap_email}"}
			,$settings{spamtrap_email}
		)
		,$cgi->comment('Do not send email to this address!!!')
	) if exists $settings{spamtrap_email};

	print $cgi->div(
		{-id => 'top'}
		,$cgi->a({-href => "$settings{base_dir}/"}
			,$cgi->img({-src => "$settings{base_dir}/gfx/links.jpg", => -width => '1200px', -height => '96px', -alt => ''})
		)
	);

		print $cgi->script({-type => 'text/javascript'}, "function f(a){window.location.href='$settings{base_dir}/'+a;}");
		print $cgi->start_div({-id => 'menu'});
	if ($user_id) {
		print $cgi->ul(
			$cgi->li({-onClick => 'f(\'\')'},		'Home')
			,$cgi->li(
				'Bookmarks'
				,$cgi->ul(
#					 $cgi->li({-onClick => 'f(\'Search\')'},	'Search')
					 $cgi->li({-onClick => 'f(\'Add\')'},		'Add')
					,$cgi->li({-onClick => 'f(\'Tagless\')'},	'Tagless')
					,$cgi->li({-onClick => 'f(\'Trash\')'},		'Trash')
					,$cgi->li({-onClick => 'f(\'Import\')'},	'Import')
					,$cgi->li({-onClick => 'f(\'?pub=1\')'},	'Public')
#					,$cgi->li({-onClick => 'f(\'Export\')'},	'Export')
#					,$cgi->li({-onClick => 'f(\'Del\')'},		'Delete')
				)
			)
			,$cgi->li(
				'Settings'
				,$cgi->ul(
					 $cgi->li({-onClick => 'f(\'Preferences\')'},	'Preferences')
					,$cgi->li({-onClick => 'f(\'UserData\')'},		'User Data')
					,$cgi->li({-onClick => 'f(\'ChPass\')'},		'Change password')
					,$cgi->li({-onClick => 'f(\'ChEmail\')'},		'Change e-mail address')
					,$cgi->li({-onClick => 'f(\'DeleteAccount\')'},	'Delete account')
				)
			)
			,$cgi->li({-onClick => 'f(\'Logout\')'},		'Logout')
		);
	} else {
		if ($settings{allow_register_new_users}) {
			print $cgi->ul(
				$cgi->li({-onClick => 'f(\'Login\')'},		'Login'),
				$cgi->li({-onClick => 'f(\'Register\')'},	'Register')
			);
		} else {
			print $cgi->ul($cgi->li({-onClick => 'f(\'Login\')'},	'Login'));
		}
	}

	print $cgi->start_form(-method => 'POST', -action => "$settings{base_dir}/Search", -enctype => 'multipart/form-data', -id => 'search');
	print $cgi->textfield(
		 -name		=> 'search'
		,-id		=> 'search_text'
		,-default	=> 'Quick search'
		,-onFocus	=> "me=document.getElementById('search_text');if(me.value=='Quick search')me.value='';"
		,-onKeyDown	=> "if(event.keyCode==13)document.forms['search'].submit();"
		,-onBlur	=> "me=document.getElementById('search_text');if(me.value=='')me.value='Quick search';"
	);
	print $cgi->end_form();

	print $cgi->end_div(); # div#menu

	print $cgi->div({id => 'bug'}, $cgi->a({-href => 'https://bugs.bsdroot.lv/enter_bug.cgi?product=Linx'}, 'Report bug')) if $user_id;
} # 2}}}

sub print_tail() { # {{{2
	print $cgi->div({-id => 'bottom'}, 'Generated in', (sprintf '%.5f', (Time::HiRes::time() - $exec_start_time)), 'seconds');
	print $cgi->div(
		{-id => 'copyright'}
		,$cgi->p("$linx{name} v$linx{version}")
		,$cgi->p($linx{copyright})
	);
	print $cgi->end_div(); # div.outer
	print $cgi->end_html;
	print "\n";
} # 2}}}

sub print_tagmap() { # {{{2
	my ($stm, $row, $res);
	unless ($pub) {
		$stm = $db->prepare('SELECT tag, links FROM (SELECT tag, COUNT(link_id) AS links FROM tagmap WHERE user_id = ? GROUP BY tag) AS s WHERE links > ? ORDER BY tag');
		$res = $stm->execute($user_id, $user_settings{min_links_for_tag} - 1);
	} else {
		$stm = $db->prepare('SELECT tag, links FROM (SELECT tag, COUNT(link_id) AS links FROM tagmap GROUP BY tag) AS s WHERE links > ? ORDER BY tag');
		$res = $stm->execute($settings{min_pub_links_for_tag} - 1);
	}
	if ($res != 0E0) {
		my (@tags, %tag_links);

			my $max = 0;
			while ($row = $stm->fetch()) {
				push(@tags, $row->[0]);
				$tag_links{$row->[0]} = $row->[1] - $user_settings{min_links_for_tag};
				$max = $row->[1] - $user_settings{min_links_for_tag} if $max < $row->[1];
			}

			print $user_settings{tagmap_on_top} ? $cgi->start_div({-id => 'top_tagmap'}) : $cgi->start_div({-id => 'tagmap'});
			foreach my $tag (@tags) {
				my $ratio = $tag_links{$tag} / $max;

				if		($ratio > 0.7)	{ print $cgi->a({-class => 't0', -href => "$settings{base_dir}/$tag"}, $tag); }
				elsif	($ratio > 0.3)	{ print $cgi->a({-class => 't1', -href => "$settings{base_dir}/$tag"}, $tag); }
				elsif	($ratio > 0.2)	{ print $cgi->a({-class => 't2', -href => "$settings{base_dir}/$tag"}, $tag); }
				elsif	($ratio > 0.1)	{ print $cgi->a({-class => 't3', -href => "$settings{base_dir}/$tag"}, $tag); }
				elsif	($ratio > 0.05)	{ print $cgi->a({-class => 't4', -href => "$settings{base_dir}/$tag"}, $tag); }
				elsif	($ratio > 0.03)	{ print $cgi->a({-class => 't5', -href => "$settings{base_dir}/$tag"}, $tag); }
				elsif	($ratio > 0.02)	{ print $cgi->a({-class => 't6', -href => "$settings{base_dir}/$tag"}, $tag); }
				else					{ print $cgi->a({-class => 't7', -href => "$settings{base_dir}/$tag"}, $tag); }

				print ' '; # important!!!
			}
			print $cgi->end_div(); # div#tagmap
	}
} # 2}}}

sub print_supported_browsers() { # {{{2
	print $cgi->start_div({-id => 'browsers'});

	print $cgi->start_div({-id => 'opera'});
	print $cgi->a(
		{-href => 'http://www.opera.com/'}
		,$cgi->img(
			{
				-src => 'gfx/browsers/opera-48x48.png'
				,-width => 48
				,-height => 48
				,-alt => 'Opera'
			}
		)
	);
	print $cgi->div({-id => 'opera_info'}, 'Works best with Opera - the leading standards supporting web browser');
	print $cgi->end_div(); #div#opera


	print $cgi->start_div({-id => 'firefox'});
	print $cgi->a(
		{-href => 'http://www.mozilla.com/firefox/'}
		,$cgi->img(
			{
				-src => 'gfx/browsers/firefox-48x48.png'
				,-width => 48
				,-height => 48
				,-alt => 'Firefox'
			}
		)
	);
	print $cgi->div({id => 'firefox_info'}, 'Works with Firefox - the leading open source web browser');
	print $cgi->end_div(); #div#firefox


	print $cgi->start_div({-id => 'safari'});
	print $cgi->a(
		{-href => 'http://www.apple.com/safari/'}
		,$cgi->img(
			{
				-src => 'gfx/browsers/safari-48x48.png'
				,-width => 48
				,-height => 48
				,-alt => 'Safari'
			}
		)
	);
	print $cgi->div({id => 'safari_info'}, 'Works well with Safari web browser');
	print $cgi->end_div(); #div#safari


	print $cgi->start_div({-id => 'chrome'});
	print $cgi->a(
		{-href => 'http://www.google.com/chrome'}
		,$cgi->img(
			{
				-src => 'gfx/browsers/chrome-48x48.png'
				,-width => 48
				,-height => 48
				,-alt => 'Safari'
			}
		)
	);
	print $cgi->div({id => 'chrome_info'}, 'Works with Google Chrome web browser');
	print $cgi->end_div(); #div#chrome



	print $cgi->end_div(); # div#browsers

} # 2}}}

sub print_postgresql_queries() { # {{{2
	print <<EOF
CREATE TABLE users (
    username        VARCHAR($field_limits{username_max_len}) NOT NULL UNIQUE CHECK (username = LOWER(username))
    ,password       CHAR($field_limits{password_max_len}) NOT NULL CHECK (password <> '$field_limits{password_null_hash}')
    ,email          VARCHAR($field_limits{email_max_len}) NOT NULL UNIQUE
    ,name           VARCHAR($field_limits{name_max_len}) NOT NULL
    ,surname        VARCHAR($field_limits{surname_max_len}) NOT NULL
    ,bdate          DATE NOT NULL CHECK (AGE(bdate) >= '$settings{min_age_required} years') -- birth date
    ,sex            CHAR(1) NOT NULL CHECK (sex = 'F' OR sex = 'M')
    ,city           VARCHAR(64) NOT NULL
    ,country        CHAR(3) NOT NULL
    ,active         BOOLEAN DEFAULT false
    ,activated      BOOLEAN DEFAULT false
    ,settings       TEXT
    ,reg_ip         INET NOT NULL DEFAULT '0.0.0.0'
    ,last_ip        INET NOT NULL DEFAULT '0.0.0.0'
    ,last_visit     TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
    ,reg_date       TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now() -- registration date
    ,id             BIGSERIAL PRIMARY KEY
);

CREATE TABLE user_requests (
    user_id         BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE
    ,request        VARCHAR($field_limits{request_max_len}) NOT NULL
    ,confirm_key    CHAR($field_limits{confirm_key_max_len}) NOT NULL
    ,data           VARCHAR($field_limits{request_data_max_len}) NOT NULL DEFAULT ''
    ,req_date       TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
    ,id             BIGSERIAL PRIMARY KEY
    ,UNIQUE(user_id, request, confirm_key)
);

CREATE TABLE links (
    url             VARCHAR($field_limits{url_max_len}) NOT NULL
    ,title          VARCHAR($field_limits{url_title_max_len}) NOT NULL
    ,description    VARCHAR($field_limits{url_description_max_len})
    ,user_id        BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE
    ,public         BOOLEAN DEFAULT true -- Public?
    ,id             BIGSERIAL PRIMARY KEY
    ,vdate          TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now() -- Last visit date&time
    ,deleted        BOOLEAN NOT NULL DEFAULT false -- Is bookmark in trash?
    ,cdate          TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now() -- Creation date&time
    ,hits           INTEGER NOT NULL DEFAULT 0 -- Hits
    ,UNIQUE(url, user_id)
);

CREATE TABLE tagmap (
    tag             VARCHAR($field_limits{tag_max_len}) NOT NULL CHECK (tag = lower(tag))
    ,link_id        BIGINT NOT NULL REFERENCES links(id) ON DELETE CASCADE
    ,user_id        BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE
    ,PRIMARY KEY (user_id,tag,link_id)
);

CREATE TABLE aliases (
    alias           VARCHAR($field_limits{alias_max_len}) NOT NULL
    ,link_id        BIGINT NOT NULL REFERENCES links(id) ON DELETE CASCADE
    ,user_id        BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE
    ,PRIMARY KEY (user_id,alias)
);


-- vim: set ts=4 sw=4:
EOF
;
	exit;
} # 2}}}

# 1}}}

# vim: set ts=4 sw=4 fdm=marker fml=1:
