#!/bin/sh

URL="http://ln.desktop.pc/"
cookie_="SID=8a0841a41a008a9723ddee5bfa033b42"
#post_="url_title=FreeBSD&url_description=FreeBSD homepage&url=http://www.freebsd.prg/&public=true&url_tags=freebsd os&submit=ok"
export SERVER_PORT="80"

cgi_name="./Linx.pl"

#=================================================

get_=`echo "$URL" | sed -e s'/.*?//'`
post=$(echo "$post_" | sed -f urlencode.sed)

if [ "$post_" ]; then
	export REQUEST_METHOD="POST"
else
	export REQUEST_METHOD="GET"
fi
export SERVER_PROTOCOL=`echo $URL | sed -e 's#://.*$##'`
export SERVER_NAME=`echo $URL | sed -E -e 's#^.+//##' -e 's#/.*$##'`
export PATH_INFO=`echo $URL | sed -E -e 's#\?.*##' -e 's#^.*://##' -e "s#^$SERVER_NAME##"`
export CONTENT_LENGTH=`expr "$post" : '.*'`
export QUERY_STRING=$(echo "$get_" | sed -f urlencode.sed)
export HTTP_COOKIE="$cookie_"

echo $post | $cgi_name
